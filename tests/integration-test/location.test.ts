import "jest";
import request from "supertest";
import { app } from "../../src/app";
import { randomUUID } from "crypto";
import { LocalOrganisation } from "../../src/model/LocalOrganisation";
import { OperatorOrganisation } from "../../src/model/OperatorOrganisation";
import { ServiceProviderOrganisation } from "../../src/model/ServiceProviderOrganisation";
import { AppDataSource } from "../../src/repository/Database";
import { getServiceOrganisation } from "../faker/EmspOrganisationService";
import { getLCP } from "../faker/LocalOrganisationService";
import { getOperator } from "../faker/OrganisationService";
import { X_REQUEST_ID } from "../helper/config";
import { getLocation } from "../faker/LocationService";
import { Location } from "../../src/model/Location";

describe("Location Integration Tests", () => {
    var operator: OperatorOrganisation;
    var lcp: LocalOrganisation;
    var emsp: ServiceProviderOrganisation;
    var location: Location;
    beforeAll(async () => {
      await AppDataSource.initialize();
      operator = getOperator();
      lcp = getLCP(operator);
      emsp = getServiceOrganisation();
      location = await getLocation(lcp, operator)
    });
    afterAll(async () => {
      await AppDataSource.destroy();
    });

    it("When Create Location, should Return 201 Success", async () => {
        const requestId = randomUUID();
        await request(app)
          .put("/location/create")
          .set("Accept", "application/json")
          .set(X_REQUEST_ID, requestId)
          .send(location)
          .expect((res: request.Response) => {
            console.log(res.text, 'Location Created');
            expect(res.statusCode).toBe(201); 
            expect(res.headers["x-request-id"]).toBe(requestId);
            expect(res.body.data.id).toBeGreaterThan(0);
            expect(res.body.data.shortName).toEqual(location.shortName);
          });
      });
      
      it("When Get Location Should Return 200 Success", async () => {
        const requestId = randomUUID();
        await request(app)
          .get("/location/list")
          .set("Accept", "application/json")
          .set(X_REQUEST_ID, requestId)
          .expect((res: request.Response) => {
            expect(res.statusCode).toBe(200);
            expect(res.headers[X_REQUEST_ID]).toBe(requestId);
            expect(res.body.data).toHaveLength(1);
            expect(res.body.data[0].shortName).toEqual(location.shortName);
          });
      });


      it("When Get 1 Location Should Return 200 Success", async () => {
        const requestId = randomUUID();
        await request(app)
          .get("/location/list/:id")
          .set("Accept", "application/json")
          .set(X_REQUEST_ID, requestId)
          .query({ id: location.locationId })
          .expect((res: request.Response) => {
            expect(res.statusCode).toBe(200);
            expect(res.headers[X_REQUEST_ID]).toBe(requestId);
            // expect(res.body.data[0]).toHaveLength(1)
            console.log(res.body.data, 'Location Through ID');
            expect(res.body.data.shortName).toEqual(location.shortName);
          });
      });

      it("When Updating Location Should Return 200 Success", async () => {
        const body = {
          shortName: "GOEGO",
        };
        const requestId = randomUUID();
        await request(app)
          .patch("/location/list/:id")
          .set("Accept", "application/json")
          .set(X_REQUEST_ID, requestId)
          .send(body)
          .query({ id: location.locationId })
          .expect((res: request.Response) => {
            console.log(res.text, "Location Update Ho Raha Hai");
            expect(res.statusCode).toBe(200);
            expect(res.headers[X_REQUEST_ID]).toBe(requestId);
            // expect(res.body.data).toHaveLength(0)
            console.log(res.body.data.shortName, "res body");
            console.log(location, "Updated Location Object");
    
            expect(res.body.data.shortName).toEqual(body.shortName);
          });
      });

      // DELETE LOCATION

      it("When Delete Location Should Return 200 Success", async () => {
        await request(app)
          .delete("/location/list/:id")
          .set("Accept", "application/json")
          .set("X-Request-ID", randomUUID())
          .query({ id: location.locationId })
          .expect((res: request.Response) => {
            console.log(res.text, "Location Delete Ho Gya");
          });
      });
});


  