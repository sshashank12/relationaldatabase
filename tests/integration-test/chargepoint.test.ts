import { randomUUID } from "crypto";
import "jest";
import request from "supertest";
import { app } from "../../src/app";
import { ChargePoint } from "../../src/model/ChargePoint";
import { ChargerConfiguration } from "../../src/model/ChargerConfiguration";
import { LocalOrganisation } from "../../src/model/LocalOrganisation";
import { Location } from "../../src/model/Location";
import { OperatorOrganisation } from "../../src/model/OperatorOrganisation";
import { AppDataSource } from "../../src/repository/Database";
import { getChargerConfiguration } from "../faker/ChargeConfigurationService";
import { getChargePoint } from "../faker/ChargePointService";
import { getLCP } from "../faker/LocalOrganisationService";
import { getLocation } from "../faker/LocationService";
import { getOperator } from "../faker/OrganisationService";
import { X_REQUEST_ID } from "../helper/config";

describe("ChargePoint Integration Tests", () => {
    var chargepoint: ChargePoint;
    var location: Location;
    var operator: OperatorOrganisation;
    var lcp: LocalOrganisation;
    var chargerconfig: ChargerConfiguration;
    beforeAll(async () => {
      await AppDataSource.initialize();
      operator = getOperator();
      lcp = getLCP(operator);
      chargerconfig = await getChargerConfiguration()
      location = await getLocation(lcp, operator, chargerconfig)
      chargepoint = await getChargePoint(location, chargerconfig);
    });
    afterAll(async () => {
      await AppDataSource.destroy();
    });

    it("When Create ChargePoint, should Return 201 Success", async () => {
        const requestId = randomUUID();
        await request(app)
          .put("/chargepoint/cp/create")
          .set("Accept", "application/json") 
          .set(X_REQUEST_ID, requestId)
          .send(chargepoint)
          .expect((res: request.Response) => {
            console.log(res.text, 'Charge Point Created');
            expect(res.statusCode).toBe(201); 
            expect(res.headers[X_REQUEST_ID]).toBe(requestId);
            expect(res.body.data.id).toBeGreaterThan(0);
            expect(res.body.data.chargePointName).toEqual(chargepoint.chargePointName);
          });
      });

      it("When Get Charge Point Should Return 200 Success", async () => {
        const requestId = randomUUID();
        await request(app)
          .get("/chargepoint/list")
          .set("Accept", "application/json")
          .set(X_REQUEST_ID, requestId)
          .expect((res: request.Response) => {
            expect(res.statusCode).toBe(200);
            expect(res.headers[X_REQUEST_ID]).toBe(requestId);
            expect(res.body.data).toHaveLength(1);
            console.log(res.body.data.chargePointName, 'cp name');
            console.log(chargepoint, "Updated CP Object");
            // expect(res.body.data.chargePointName).toEqual(chargepoint.chargePointName); 
          });
      });

      it("When Updating Location Should Return 200 Success", async () => {
        const body = {
          chargePointName: "goego",
        };
        const requestId = randomUUID();
        await request(app)
          .patch("/chargepoint/list/:id")
          .set("Accept", "application/json")
          .set(X_REQUEST_ID, requestId)
          .send(body)
          .query({ id: chargepoint.chargePointId })
          .expect((res: request.Response) => {
            console.log(res.text, "ChargePoint Update Ho Raha Hai");
            expect(res.statusCode).toBe(200);
            expect(res.headers[X_REQUEST_ID]).toBe(requestId);
            // expect(res.body.data).toHaveLength(0)
            console.log(res.body.data.shortName, "res body");
            console.log(location, "Updated Location Object");
    
            expect(res.body.data.chargePointName).toEqual(body.chargePointName);
          });
      });

      it("When Delete Chargepoint Should Return 200 Success", async () => {
        await request(app)
          .delete("/chargepoint/list/:id")
          .set("Accept", "application/json")
          .set("X-Request-ID", randomUUID())
          .query({ id: chargepoint.chargePointId })
          .expect((res: request.Response) => {
            console.log(res.text, "ChargePoint Delete Ho Gya");
          });
      });

});