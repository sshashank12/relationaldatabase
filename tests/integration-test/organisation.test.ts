import "jest";
import request from "supertest";
import { app } from "../../src/app";
import { AppDataSource } from "../../src/repository/Database";
import { randomUUID } from "crypto";
import { OperatorOrganisation } from "../../src/model/OperatorOrganisation";
import { LocalOrganisation } from "../../src/model/LocalOrganisation";
import { getOperator } from "../faker/OrganisationService";
import { getLCP } from "../faker/LocalOrganisationService";
import { X_REQUEST_ID } from "../helper/config";
import { ServiceProviderOrganisation } from "../../src/model/ServiceProviderOrganisation";
import { getServiceOrganisation } from "../faker/EmspOrganisationService";

describe("Organisation Integration Tests", () => {
  var operator: OperatorOrganisation;
  var lcp: LocalOrganisation;
  var emsp: ServiceProviderOrganisation;
  beforeAll(async () => {
    await AppDataSource.initialize();
    operator = getOperator();
    lcp = getLCP(operator);
    emsp = getServiceOrganisation();
  });
  afterAll(async () => {
    await AppDataSource.destroy();
  });

  // Create CPO (Passed)
  it("When Create CPO, should Return 201 Success", async () => {
    const requestId = randomUUID();
    await request(app)
      .put("/organisation/cpo/create")
      .set("Accept", "application/json")
      .set(X_REQUEST_ID, requestId)
      .send(operator)
      .expect((res: request.Response) => {
        expect(res.statusCode).toBe(201);
        expect(res.headers["x-request-id"]).toBe(requestId);
        expect(res.body.data.id).toBeGreaterThan(0);
        expect(res.body.data.fullName).toEqual(operator.fullName);
      });
  });

  // GET All CPO (Passed)

  it("When Get CPO Should Return 200 Success", async () => {
    const requestId = randomUUID();
    await request(app)
      .get("/organisation/cpo")
      .set("Accept", "application/json")
      .set(X_REQUEST_ID, requestId)
      .expect((res: request.Response) => {
        expect(res.statusCode).toBe(200);
        expect(res.headers[X_REQUEST_ID]).toBe(requestId);
        expect(res.body.data).toHaveLength(1);
        expect(res.body.data[0].fullName).toEqual(operator.fullName);
      });
  });

  // GET 1 CPO Through ID (Passed)

  it("When Get 1 CPO Should Return 200 Success", async () => {
    const requestId = randomUUID();
    await request(app)
      .get("/organisation/cpo/:id")
      .set("Accept", "application/json")
      .set(X_REQUEST_ID, requestId)
      .query({ id: "1" })
      .expect((res: request.Response) => {
        expect(res.statusCode).toBe(200);
        expect(res.headers[X_REQUEST_ID]).toBe(requestId);
        // expect(res.body.data).toHaveLength(0)
        expect(res.body.data.fullName).toEqual(operator.fullName);
      });
  });

  // UPDATING CPO (Passed)

  it("When Updating CPO Should Return 200 Success", async () => {
    const body = {
      shortName: "Anind",
    };
    const requestId = randomUUID();
    await request(app)
      .patch("/organisation/cpo/:id")
      .set("Accept", "application/json")
      .set(X_REQUEST_ID, requestId)
      .send(body)
      .query({ id: "1" })
      .expect((res: request.Response) => {
        console.log(res.text, "CPO Update Ho Raha Hai");
        expect(res.statusCode).toBe(200);
        expect(res.headers[X_REQUEST_ID]).toBe(requestId);
        // expect(res.body.data).toHaveLength(0)
        console.log(res.body.data.shortName, "res body");
        console.log(operator, "operator object");

        expect(res.body.data.shortName).toEqual(body.shortName);
      });
  });

  // GET CPO After Update (Passed)

  it("When Get CPO After Update Should Return 200 Success", async () => {
    const requestId = randomUUID();
    const shortName = "Anind";
    await request(app)
      .get("/organisation/cpo/:id")
      .set("Accept", "application/json")
      .set(X_REQUEST_ID, requestId)
      .query({ id: "1" })
      .expect((res: request.Response) => {
        expect(res.statusCode).toBe(200);
        expect(res.headers[X_REQUEST_ID]).toBe(requestId);
        // expect(res.body.data).toHaveLength(0)
        console.log(res.body.data.shortName, "res body after update");
        console.log(operator, "operator object afdter update");
        // expect(operator.shortName).toEqual(operator.shortName)
        console.log(res.text, "CPO Update Ke Baad Aa Raha Hai");
      });
  });

  // DELETE CPO (Passed)

  it("When Delete CPO Should Return 200 Success", async () => {
    await request(app)
      .delete("/organisation/cpo/:id")
      .set("Accept", "application/json")
      .set("X-Request-ID", randomUUID())
      .query({ id: "1" })
      .expect((res: request.Response) => {
        console.log(res.text, "CPO Delete Ho Gya");
      });
  });

  // CREATE LCP

  it("When Create LCP Should Return 200 Success", async () => {
    await request(app)
      .put("/organisation/lcp/create")
      .set("Accept", "application/json")
      .set("X-Request-ID", randomUUID())
      .send(lcp)
      .expect((res: request.Response) => {
        console.log(res.text, "LCP Created");
      });
  });

  // GET All LCP ()

  it("When Get LCP Should Return 200 Success", async () => {
    await request(app)
      .get("/organisation/lcp")
      .set("Accept", "application/json")
      .set("X-Request-ID", randomUUID())
      .expect((res: request.Response) => {
        console.log(res.text, "Sab LCP Aa Raha Hai");
      });
  });

  //GET One LCP Through ID

  it("When Get 1 LCP Should Return 200 Success", async () => {
    await request(app)
      .get("/organisation/lcp/:id")
      .set("Accept", "application/json")
      .set("X-Request-ID", randomUUID())
      .query({ id: "2" })
      .expect((res: request.Response) => {
        console.log(res.text, "LCP ID Ke Through Aa Raha Hai");
      });
  });

  // UPDATE LCP (Passed)

  it("When Updating LCP Should Return 200 Success", async () => {
    const body = {
      shortName: "Rahul",
    };
    const requestId = randomUUID();
    await request(app)
      .patch("/organisation/lcp/:id")
      .set("Accept", "application/json")
      .set("X-Request-ID", requestId)
      .send(body)
      .query({ id: "2" })
      .expect((res: request.Response) => {
        console.log(res.text, "LCP Update Ho Raha Hai");
        expect(res.statusCode).toBe(200);
        expect(res.headers[X_REQUEST_ID]).toBe(requestId);
        expect(res.body.data.shortName).toEqual(body.shortName);
      });
  });

  // GET LCP After Update (Passed)

  it("When Get LCP After Update Should Return 200 Success", async () => {
    const requestId = randomUUID();
    await request(app)
      .get("/organisation/lcp")
      .set("Accept", "application/json")
      .set("X-Request-ID", requestId)
      .expect((res: request.Response) => {
        console.log(res.text, "LCP Update Ke Baad Aa Raha Hai");
        // expect(res.body.data).toHaveLength(0)
        console.log(res.body.data.shortName, "res body LCP after update");
        console.log(lcp, "lcp object afdter update");
        expect(res.body.data.shortName).toEqual("Rahul");
      });
  });

  // DELETE LCP (Passed)

  it("When Delete LCP Should Return 200 Success", async () => {
    await request(app)
      .delete("/organisation/lcp/:id")
      .set("Accept", "application/json")
      .set("X-Request-ID", randomUUID())
      .query({ id: "1" })
      .expect((res: request.Response) => {
        console.log(res.text, "LCP Delete Ho Gya");
      });
  });

  // CREATE EMSP
  it("When Create EMSP, should Return 201 Success", async () => {
    const requestId = randomUUID();
    await request(app)
      .put("/organisation/emsp/create")
      .set("Accept", "application/json")
      .set(X_REQUEST_ID, requestId)
      .send(emsp)
      .expect((res: request.Response) => {
        console.log(res.text, "EMSP Created");
        expect(res.statusCode).toBe(201);
        expect(res.headers["x-request-id"]).toBe(requestId);
        expect(res.body.data.id).toBeGreaterThan(0);
        expect(res.body.data.fullName).toEqual(emsp.fullName);
      });
  });

  // GET EMSP
  it("When Get EMSP Should Return 200 Success", async () => {
    const requestId = randomUUID();
    await request(app)
      .get("/organisation/emsp")
      .set("Accept", "application/json")
      .set("X-Request-ID", requestId)
      .expect((res: request.Response) => {
        console.log(res.text, "Sab EMSP Aa Raha Hai");
      });
  });

  //GET EMSP Through ID

  it("When Get 1 EMSP Should Return 200 Success", async () => {
    const requestId = randomUUID();
    await request(app)
      .get("/organisation/emsp/:id")
      .set("Accept", "application/json")
      .set(X_REQUEST_ID, requestId)
      .query({ id: "3" })
      .expect((res: request.Response) => {
        expect(res.statusCode).toBe(200);
        expect(res.headers[X_REQUEST_ID]).toBe(requestId);
        // expect(res.body.data).toHaveLength(0)
        console.log(emsp, "emsp Object");
        expect(res.body.data.fullName).toEqual(emsp.fullName);
      });
  });

  // Update EMPS

  it("When Updating EMSP Should Return 200 Success", async () => {
    const body = {
      shortName: "Siddhant",
    };
    const requestId = randomUUID();
    await request(app)
      .patch("/organisation/emsp/:id")
      .set("Accept", "application/json")
      .set("X-Request-ID", requestId)
      .send(body)
      .query({ id: "3" })
      .expect((res: request.Response) => {
        console.log(res.text, "EMSP Update Ho Raha Hai");
        expect(res.statusCode).toBe(200);
        expect(res.headers[X_REQUEST_ID]).toBe(requestId);
        expect(res.body.data.shortName).toEqual(body.shortName);
      });
  });

  // GET EMSP After Update (Passed)

  it("When Get EMSP After Update Should Return 200 Success", async () => {
    const requestId = randomUUID();
    await request(app)
      .get("/organisation/emsp")
      .set("Accept", "application/json")
      .set("X-Request-ID", requestId)
      .expect((res: request.Response) => {
        console.log(res.text, "EMSP Update Ke Baad Aa Raha Hai");
        // expect(res.body.data).toHaveLength(0)
        console.log(res.body.data[0].shortName, "emsp body after update");
        console.log(emsp.shortName, "emsp object afdter update");

        expect(res.body.data[0].shortName).toEqual("Siddhant");
      });
  });

  // DELETE EMSP (Passed)

  it("When Delete EMSP Should Return 200 Success", async () => {
    await request(app)
      .delete("/organisation/emsp/:id")
      .set("Accept", "application/json")
      .set("X-Request-ID", randomUUID())
      .query({ id: "3" })
      .expect((res: request.Response) => {
        console.log(res.text, "EMSP Delete Ho Gya");
      });
  });
});
