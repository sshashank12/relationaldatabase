import { randomUUID } from "crypto";
import request from "supertest";
import "jest"
import { ChargePoint } from "../../src/model/ChargePoint";
import { ChargerConfiguration } from "../../src/model/ChargerConfiguration";
import { LocalOrganisation } from "../../src/model/LocalOrganisation";
import { OperatorOrganisation } from "../../src/model/OperatorOrganisation";
import { AppDataSource } from "../../src/repository/Database";
import { getChargerConfiguration } from "../faker/ChargeConfigurationService";
import { getChargePoint } from "../faker/ChargePointService";
import { getLCP } from "../faker/LocalOrganisationService";
import { getLocation } from "../faker/LocationService";
import { getOperator } from "../faker/OrganisationService";
import { app } from "../../src/app";
import { X_REQUEST_ID } from "../helper/config";


describe("Charger Configuration Integration Tests", () => {
    var chargerconfig: ChargerConfiguration
    beforeAll(async () => {
      await AppDataSource.initialize();
      chargerconfig = await getChargerConfiguration()
    });
    afterAll(async () => {
      await AppDataSource.destroy();
    });

    it("When Create Charger Configuration, should Return 201 Success", async () => {
      const requestId = randomUUID();
      await request(app)
        .put("/chargerconfig/create")
        .set("Accept", "application/json") 
        .set(X_REQUEST_ID, requestId)
        .send(chargerconfig)
        .expect((res: request.Response) => {
          console.log(res.text, 'Charger Configuration Created');
          expect(res.statusCode).toBe(201); 
          expect(res.headers[X_REQUEST_ID]).toBe(requestId);
          expect(res.body.data.id).toBeGreaterThan(0);
          expect(res.body.data.configName).toEqual(chargerconfig.configName);
        });
    });



  });