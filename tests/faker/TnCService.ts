import { faker } from "@faker-js/faker";
import { TermsAndCondition } from "../../src/model/TermsAndCondition";

export  function createTnC() {
  const tnc = new TermsAndCondition();
  tnc.version = faker.datatype.number();
  tnc.acceptedOn = faker.datatype.datetime();
  tnc.createdBy = faker.name.firstName();
  tnc.updatedBy = faker.name.firstName();
  tnc.updatedByUser = faker.name.firstName();
  tnc.createdByUser = faker.name.firstName();
  tnc.createdAt = faker.date.past();
  return tnc;
}