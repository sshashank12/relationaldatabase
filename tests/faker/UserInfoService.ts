import { faker } from "@faker-js/faker";
import { UserInfo } from "../../src/model/UserInfo";

export function createUserInfo() {
    const userInfo = new UserInfo();
    userInfo.emailVerified = faker.datatype.boolean();
    userInfo.role = faker.name.jobType();
    userInfo.access = faker.name.jobType();
    userInfo.createdBy = faker.name.firstName();
    userInfo.updatedBy = faker.name.firstName();
    userInfo.updatedByUser = faker.name.firstName();
    userInfo.createdByUser = faker.name.firstName();
    userInfo.createdAt = faker.date.past();
    return userInfo;
}
