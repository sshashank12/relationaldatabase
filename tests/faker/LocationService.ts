import {
  Facility,
  Location,
  LocationStatus,
  LocationType,
  ParkingType,
} from "../../src/model/Location";
import { faker } from "@faker-js/faker";
import { createGeoLocation } from "./GeoLocationService";
import { createContact } from "./ContactService";
import { createContractInfo } from "./ContractInfoService";
import { getChargePoint } from "./ChargePointService";
import { createAddress } from "./AddressServices";
import { LocalOrganisation } from "../../src/model/LocalOrganisation";
import { createChargingSession } from "./ChargingSessionServices";
import { OperatorOrganisation } from "../../src/model/OperatorOrganisation";
import { TariffRateCard } from "../../src/model/TariffRateCard";
import { Review } from "../../src/model/Review";
import { AppDataSource } from "../../src/repository/Database";
import { ChargerConfiguration } from "../../src/model/ChargerConfiguration";

export async function getLocation(localorg: LocalOrganisation, org: OperatorOrganisation, chargeconfig: ChargerConfiguration) {
  const location = new Location();
  location.docId = faker.datatype.uuid();
  location.locationId = faker.random.alphaNumeric(16);
  location.shortName = faker.random.alphaNumeric(5);
  location.shortCode = faker.random.alpha({ count: 5, casing: "upper" });
  location.showOnMap = faker.datatype.boolean();
  location.name = faker.random.word();
  location.address = createAddress();
  location.coordinates = createGeoLocation();
  location.parkingType = ParkingType.ALONG_MOTORWAY;
  // const orgEntity = await AppDataSource.getRepository(OperatorOrganisation).findOneOrFail({ where: { id: org.id } });
  const orgEntity = await AppDataSource.getRepository(OperatorOrganisation).createQueryBuilder().where("id = :id", { id: org.id }).execute()
  location.operator = orgEntity;
  location.suboperator = orgEntity;
  const localEntity = await AppDataSource.getRepository(LocalOrganisation).createQueryBuilder().where("id = :id", { id: localorg.id }).execute()
  location.owner = localEntity;
  location.contact = createContact();
  location.contractInfo = createContractInfo();
  location.amenities = [Facility.AIRPORT, Facility.PARKING_LOT];
  location.type = LocationType.CAPTIVE;
  location.status = LocationStatus.ACTIVE;
  location.chargepoints = [await getChargePoint(location, chargeconfig)];
  const reviewEntity = await AppDataSource.getRepository(Review).find({
    loadRelationIds: true,
    order: { createdAt: "DESC" },
  });
  if (reviewEntity === null) throw new Error();
  location.reviews = [reviewEntity[0]];
  location.last_updated = faker.datatype.datetime();
  location.createdBy = faker.name.firstName();
  location.updatedBy = faker.name.firstName();
  location.updatedByUser = faker.name.firstName();
  location.createdByUser = faker.name.firstName();
  location.createdAt = faker.date.past();
  return location;
}
