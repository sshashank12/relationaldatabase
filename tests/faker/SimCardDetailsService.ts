import { faker } from "@faker-js/faker";
import { ChargePoint } from "../../src/model/ChargePoint";
import { SimCardDetail } from "../../src/model/SimCardDetails";

export async function createSimCardDetails(cp: ChargePoint) {
  const SimCarddetail = new SimCardDetail();
  SimCarddetail.chargePoint = cp;
  SimCarddetail.simCardNumber = faker.datatype.number();
  SimCarddetail.mobileNumber = faker.datatype.number();
  SimCarddetail.simProvider = faker.random.word();
  SimCarddetail.simType = faker.finance.currencyName();
  SimCarddetail.simBillPayer = faker.random.alphaNumeric(16);
  SimCarddetail.simCardOwner = faker.name.firstName();
  SimCarddetail.createdBy = faker.name.firstName();
  SimCarddetail.updatedBy = faker.name.firstName();
  SimCarddetail.updatedByUser = faker.name.firstName();
  SimCarddetail.createdByUser = faker.name.firstName();
  SimCarddetail.createdAt = faker.date.past();
  return SimCarddetail;
}
