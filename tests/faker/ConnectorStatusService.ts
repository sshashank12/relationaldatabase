import { faker } from "@faker-js/faker";
import { ChargePoint } from "../../src/model/ChargePoint";
import { Connector } from "../../src/model/Connector";
import {
  ChargePointErrorCode,
  ConnectorStatus,
  Status,
} from "../../src/model/ConnectorStatus";
import { getChargePoint } from "./ChargePointService";
import { createConnector } from "./ConnectorService";

export function createConnectorStatus(connector: Connector, cp: ChargePoint) {
  const connstatus = new ConnectorStatus();
  connstatus.connector = connector;
  connstatus.evse = cp;
  connstatus.errorCode = ChargePointErrorCode.AD_HOC_USER;
  connstatus.info = faker.datatype.string();
  connstatus.status = Status.CHARGING;
  connstatus.timestamp = faker.datatype.datetime();
  connstatus.vendorErrorCode = faker.random.alpha({
    count: 5,
    casing: "upper",
  });
  connstatus.vendorId = faker.datatype.string();
  return connstatus;
}
