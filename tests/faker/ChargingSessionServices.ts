import { faker } from "@faker-js/faker";
import { ChargingSession, Status } from "../../src/model/ChargingSession";
import { createIDtag } from "./IDtagServices";
import { createTariff } from "./TariffService";
import { createTransaction } from "./TransactionSevices";
import { createDisputeResolution } from "./DisputeResolutionService";
import { createContractInfo } from "./ContractInfoService";
import { createRevenue } from "./RevenueService";
import { createLocation } from "./LocationService";
import { Connector } from "../../model/Connector";
import { ChargePoint } from "../../model/ChargePoint";
export async function createChargingSession(
  connector: Connector,
  cp: ChargePoint,
  org: Organisation
) {
  const chargingSession = new ChargingSession();
  chargingSession.connectorId = faker.random.alphaNumeric(16);
  chargingSession.taxableValue = faker.random.numeric();
  chargingSession.idtag = createIDtag();
  chargingSession.lastUpdatedAt = faker.date.past();
  chargingSession.startedAt = faker.date.past();
  chargingSession.lastValue = faker.datatype.number();
  chargingSession.startValue = faker.datatype.number();
  chargingSession.status = Status.ACTIVE;
  chargingSession.lastTime = faker.date.past();
  chargingSession.stopReason = faker.random.words(5);
  chargingSession.stopReasonDetail = faker.random.words(15);
  chargingSession.transaction = createTransaction();
  chargingSession.costRefund = faker.datatype.number();
  chargingSession.tariffRateCard = [await createTariff(org)];
  chargingSession.cost = faker.datatype.number();
  chargingSession.cutOffAmount = faker.datatype.number();
  chargingSession.connectionFee = faker.datatype.number();
  chargingSession.finalFixedRate = faker.datatype.number();
  chargingSession.finalPerUnitRate = faker.datatype.number();
  chargingSession.taxRate = faker.datatype.number();
  chargingSession.rateUnit = faker.random.words();
  chargingSession.rateName = faker.random.words();
  chargingSession.rateId = faker.random.words();
  chargingSession.sessionCostCashUsed = faker.datatype.number();
  chargingSession.sessionCostCreditUsed = faker.datatype.number();
  chargingSession.transactionDocId = faker.random.alphaNumeric(16);
  chargingSession.adjustmentCost = faker.datatype.number();
  chargingSession.userWalletId = faker.random.alphaNumeric(16);
  chargingSession.disputeResolution = [await createDisputeResolution()];
  chargingSession.disputeRaised = faker.datatype.boolean();
  // chargingSession.contractInfo = createContractInfo(loc);
  chargingSession.revenueSharedPerUnit = faker.datatype.number();
  chargingSession.electricityCost = faker.datatype.number();
  chargingSession.revenueSharedFixed = faker.datatype.number();
  chargingSession.revenue = createRevenue();
  chargingSession.phoneNumber = faker.phone.number();
  chargingSession.chargePointModel = faker.random.words();
  chargingSession.deductionAmount = faker.datatype.number();
  chargingSession.invoiceStatus = faker.random.words();
  chargingSession.chargePoint = cp;
  // chargingSession.location = [createLocation()]
  chargingSession.connector = connector;
  chargingSession.createdBy = faker.name.firstName();
  chargingSession.createdAt = faker.date.recent();
  chargingSession.updatedAt = faker.date.recent();
  chargingSession.updatedByUser = faker.name.firstName();
  chargingSession.monthYear = faker.random.word();
  return chargingSession;
}
