import { faker } from "@faker-js/faker";
import { OrganisationStatus, OrganisationType } from "../../src/model/Organisation";
import { ServiceProviderOrganisation } from "../../src/model/ServiceProviderOrganisation";
import { createAddress } from "./AddressServices";
import { createContact } from "./ContactService";
import { createImage } from "./ImageService";
import { createPayment } from "./PaymentService";

export function getServiceOrganisation() {
    const emsp = new ServiceProviderOrganisation()
    emsp.fullName = faker.name.firstName();
    emsp.shortName = faker.random.word();
    emsp.shortCode = faker.random.alphaNumeric(5);
    emsp.orgtype = OrganisationType.EMSP;
    emsp.status = OrganisationStatus.ACTIVE;
    emsp.companyIdNumber = faker.random.numeric();
    emsp.gstNumber = faker.random.numeric();
    emsp.contact = createContact();
    emsp.address = createAddress();
    emsp.paymentDetails = createPayment();
    emsp.website = faker.internet.url();
    emsp.logo = createImage();
    emsp.taxRate = faker.datatype.number()
    emsp.createdBy = faker.name.firstName();
    emsp.updatedBy = faker.name.firstName();
    emsp.updatedByUser = faker.name.firstName();
    emsp.createdByUser = faker.name.firstName();
    emsp.createdAt = faker.date.past();
    return emsp
}