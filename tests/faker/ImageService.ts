import { faker } from "@faker-js/faker";
import { Image, ImageCategory } from "../../src/model/Image";

export function createImage() {
  const image = new Image();
  image.url = faker.internet.url();
  image.thumbnail = faker.internet.url();
  image.category = ImageCategory.CHARGER;
  image.type = faker.random.numeric();
  image.width = faker.datatype.number();
  image.height = faker.datatype.number();

  image.createdBy = faker.name.firstName();
  image.updatedBy = faker.name.firstName();
  image.updatedByUser = faker.name.firstName();
  image.createdByUser = faker.name.firstName();
  image.createdAt = faker.date.past();

  return image;
}
