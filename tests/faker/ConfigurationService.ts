import { faker } from "@faker-js/faker";
import { Configurations } from "../../src/model/Configurations";

export function createConfiguration() {
  const config = new Configurations();
  config.readonly = faker.datatype.boolean();
  config.key = faker.datatype.string();
  config.currentValue = faker.datatype.number();
  config.defaultValue = faker.datatype.number();
  return config;
}
