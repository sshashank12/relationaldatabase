import { faker } from "@faker-js/faker";
import { Location } from "../../src/model/Location";
import { Member } from "../../src/model/Member";
import { Review } from "../../src/model/Review";

export async function createReview(loc: Location, member: Member) {
  const review = new Review();
  review.message = faker.random.words();
  review.rating = faker.datatype.number({max: 5});
  // review.user = member; 
  review.userName = faker.name.firstName();
  review.date = faker.datatype.datetime();
  review.location = loc
  review.user = member
  review.isPublic = faker.datatype.boolean();
  review.title = faker.random.word();
  review.response = faker.datatype.string(255);
  review.imagePath = faker.internet.url();
  review.createdBy = faker.name.firstName();
  review.updatedBy = faker.name.firstName();
  review.updatedByUser = faker.name.firstName();
  review.createdByUser = faker.name.firstName();
  review.createdAt = faker.date.past();
  return review;
}
