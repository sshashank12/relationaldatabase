import { faker } from "@faker-js/faker";
import { createTransaction } from "./TransactionSevices";
import { DisputeResolution } from "../../src/model/DisputeResolution";

export function createDisputeResolution() {
  const disputeResolution = new DisputeResolution();
  disputeResolution.transaction = createTransaction();
  disputeResolution.description = faker.lorem.sentence();

  return disputeResolution;
}
