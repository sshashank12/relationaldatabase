import {
  Organisation,
  OrganisationStatus,
  OrganisationType,
} from "../../src/model/Organisation";
import { faker } from "@faker-js/faker";
import { createContact } from "./ContactService";
import { createPayment } from "./PaymentService";
import { createImage } from "./ImageService";
import { createAddress } from "./AddressServices";
import { OperatorOrganisation } from "../../src/model/OperatorOrganisation";

export function getOperator() {
  const organisation = new OperatorOrganisation();
  organisation.fullName = faker.name.firstName();
  organisation.shortName = faker.random.word();
  organisation.shortCode = faker.random.alphaNumeric(5);
  organisation.orgtype = OrganisationType.CPO;
  organisation.status = OrganisationStatus.ACTIVE;
  organisation.companyIdNumber = faker.random.numeric();
  organisation.gstNumber = faker.random.numeric();
  organisation.contact = createContact();
  organisation.address = createAddress();
  organisation.paymentDetails = createPayment();
  organisation.website = faker.word.adjective();
  organisation.logo = createImage();
  organisation.createdBy = faker.name.firstName();
  organisation.updatedBy = faker.name.firstName();
  organisation.updatedByUser = faker.name.firstName();
  organisation.createdByUser = faker.name.firstName();
  organisation.createdAt = faker.date.past();
  return organisation;
}