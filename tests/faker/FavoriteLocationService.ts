import { faker } from "@faker-js/faker";
import { FavoriteLocations } from "../../src/model/FavoriteLocations";
import { Location } from "../../src/model/Location";
import { Member } from "../../src/model/Member";
import { AppDataSource } from "../../src/repository/ChargepointRepository";
import { createConnector } from "./ConnectorService";

export async function createFavoriteLocation(member: Member){
    const favloc = new FavoriteLocations()
    favloc.name = faker.name.firstName()
    favloc.user = member
    const locationEntity = await AppDataSource.getRepository(Location).find({loadRelationIds: true, order: {createdAt: 'DESC'}});
    if(locationEntity === null) throw new Error();
    favloc.location = locationEntity[0]
    favloc.createdBy = faker.name.firstName();
    favloc.createdAt = faker.date.recent();
    favloc.updatedAt = faker.date.recent();
    favloc.updatedByUser = faker.name.firstName();
    favloc.monthYear = faker.random.word();
    return favloc
}