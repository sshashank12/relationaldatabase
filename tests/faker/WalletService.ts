import { faker } from "@faker-js/faker";
import { Wallet } from "../../src/model/Wallet";

export function createWallet() {
    const wallet = new Wallet();
    wallet.balance = faker.datatype.number();
    wallet.userName = faker.name.firstName();
    wallet.phoneNumber = faker.phone.number();
    return wallet;
  }