import { Contact } from "../../src/model/Contact";
import { faker } from "@faker-js/faker";

export function createContact() {
  const contact = new Contact();
  contact.name = faker.name.firstName();
  contact.phoneNumber = faker.phone.number();
  contact.email = faker.random.alphaNumeric(16);
  contact.designation = faker.name.jobTitle();

  contact.createdBy = faker.name.firstName();
  contact.updatedBy = faker.name.firstName();
  contact.updatedByUser = faker.name.firstName();
  contact.createdByUser = faker.name.firstName();
  contact.createdAt = faker.date.past();

  return contact;
}
