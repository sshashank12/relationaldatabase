import { faker } from "@faker-js/faker";
import { Organisation } from "../../src/model/Organisation";
import { TariffRateCard, TariffTypeEnum } from "../../src/model/TariffRateCard";
import { createTariffRateCardEntry } from "./TariffRateCardEntryService";
import { createTariffRateFlatEntry } from "./TariffRateFlatEntryService";
export async function createTariff(): Promise<TariffRateCard> {
  const Tariff = new TariffRateCard();
  // Tariff.organisation = org;
  Tariff.rateUnit = TariffTypeEnum.PER_KWH;
  Tariff.name = faker.random.words();
  Tariff.version = faker.datatype.number({ max: 10 });
  Tariff.entries = [await createTariffRateCardEntry()];
  Tariff.rateEntries = [await createTariffRateFlatEntry()];
  Tariff.finalFixedRate = faker.datatype.number({ max: 100 });
  Tariff.finalPerUnitRate = faker.datatype.number({ max: 40 });
  Tariff.electricityProvider = faker.random.words();
  Tariff.consumerNumber = faker.datatype.number();
  // Tariff.chargingSession = faker.random.words();

  return Tariff;
}
