import { faker } from "@faker-js/faker";
import { ChargerConfiguration, ChargerModelType, Status } from "../../src/model/ChargerConfiguration";
import { createConfiguration } from "./ConfigurationService";

export async function getChargerConfiguration(){
    const chargerconfig = new ChargerConfiguration()
    chargerconfig.configName = faker.random.word()
    chargerconfig.controller = faker.datatype.string()
    chargerconfig.status = Status.Active
    chargerconfig.firmwareVersion = faker.datatype.number()
    chargerconfig.isDefault = faker.datatype.boolean()
    chargerconfig.chargerType = ChargerModelType.BharatAC
    chargerconfig.configuration = createConfiguration()
    chargerconfig.createdBy = faker.name.firstName();
    chargerconfig.updatedBy = faker.name.firstName();
    chargerconfig.updatedByUser = faker.name.firstName();
    chargerconfig.createdByUser = faker.name.firstName();
    chargerconfig.createdAt = faker.date.past();
    console.log(chargerconfig, 'chargeconfig');
    return chargerconfig
}