import { faker } from "@faker-js/faker";
import { cp } from "fs";
import { ChargePoint } from "../../src/model/ChargePoint";
import { WifiDetails } from "../../src/model/WifiDetails";

export function createWifiDetails(cp: ChargePoint) {
  const Wifidetails = new WifiDetails();
  Wifidetails.chargePoint = cp;
  Wifidetails.wifiSSID = faker.internet.domainWord();
  Wifidetails.wifiProvider = faker.internet.domainWord();
  Wifidetails.wifiPassword = faker.internet.password();
  Wifidetails.wifiAuthenticationType = faker.finance.currencyName();
  Wifidetails.macAddress = faker.internet.port();
  Wifidetails.createdBy = faker.name.firstName();
  Wifidetails.updatedBy = faker.name.firstName();
  Wifidetails.updatedByUser = faker.name.firstName();
  Wifidetails.createdByUser = faker.name.firstName();
  Wifidetails.createdAt = faker.date.past();
  return Wifidetails;
}
