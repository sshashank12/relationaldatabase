import { faker } from "@faker-js/faker";
import { Member } from "../../src/model/Member";
import { Vehicle } from "../../src/model/Vehicle";

export function createVehicle(member: Member) {
  const vehicle = new Vehicle();
  vehicle.user = member;
  vehicle.licensePlate = faker.vehicle.vin();
  vehicle.isActive = faker.datatype.boolean();
  vehicle.batteryCapacity = faker.datatype.number();
  vehicle.batterySwappable = faker.datatype.boolean();
  vehicle.bodyType = faker.vehicle.type();
  vehicle.chargingTime = faker.datatype.string();
  vehicle.connectorType = faker.random.word();
  vehicle.connectorTypesSupported = faker.random.word();
  vehicle.imagesPath = faker.datatype.string();
  vehicle.manufacturer = faker.vehicle.manufacturer();
  vehicle.model = faker.vehicle.model();
  vehicle.range = faker.datatype.number();
  vehicle.variant = faker.random.word();
  vehicle.vehicleId = faker.random.alphaNumeric(16);
  vehicle.wheelerType = faker.vehicle.type();
  vehicle.vehicleType = faker.vehicle.type();
  vehicle.fastChargingSupported = faker.datatype.boolean();

  vehicle.createdBy = faker.name.firstName();
  vehicle.updatedBy = faker.name.firstName();
  vehicle.updatedByUser = faker.name.firstName();
  vehicle.createdByUser = faker.name.firstName();
  vehicle.createdAt = faker.date.past();

  return vehicle;
}
