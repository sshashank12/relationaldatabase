import { faker } from "@faker-js/faker";
import { ChargePoint } from "../../src/model/ChargePoint";
import { LANDetails } from "../../src/model/LANDetails";

export function createLANDetails(cp: ChargePoint) {
  const LANdetails = new LANDetails();
  LANdetails.chargePoint = cp;
  LANdetails.macAddress = faker.datatype.number();
  LANdetails.createdBy = faker.name.firstName();
  LANdetails.updatedBy = faker.name.firstName();
  LANdetails.updatedByUser = faker.name.firstName();
  LANdetails.createdByUser = faker.name.firstName();
  LANdetails.createdAt = faker.date.past();
  return LANdetails;
}
