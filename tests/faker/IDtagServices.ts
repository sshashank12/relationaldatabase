import { IdTag, IdTagStatus, CardType } from "../../src/model/IdTag";
import { faker } from "@faker-js/faker";
faker.locale = "en_IND";

export function createIDtag() {
  const idtag = new IdTag();
  idtag.cardNumber = faker.address.buildingNumber();
  idtag.rfid = faker.address.secondaryAddress();
  idtag.status = IdTagStatus.ACTIVE;
  idtag.type = CardType.VIRTUAL;
  // idtag.user = createMember();
  idtag.batchId = faker.address.city();
  idtag.issueDate = faker.date.recent();

  return idtag;
}
