import { ContractInfo } from "../../src/model/ContractInfo";
import { faker } from "@faker-js/faker";

export function createContractInfo() {
    const contractInfo = new ContractInfo()
    contractInfo.lcpShare = faker.datatype.number({max: 100})
    contractInfo.contractOwnerShare = 100 - contractInfo.lcpShare
    return contractInfo
}

