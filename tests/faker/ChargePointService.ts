import { faker } from "@faker-js/faker";
import {
  ChargePoint,
  ChargerModelType,
  ChargingStatus,
  RegistrationStatus,
} from "../../src/model/ChargePoint";
import { Location } from "../../src/model/Location";
import { getChargerConfiguration } from "./ChargeConfigurationService";
import { createConnector } from "./ConnectorService";
import { createGeoLocation } from "./GeoLocationService";
import { LocalOrganisation } from "../../src/model/LocalOrganisation";
import { TariffRateCard } from "../../src/model/TariffRateCard";
import { createWifiDetails } from "./WifiDetailsService";
import { ChargerConfiguration } from "../../src/model/ChargerConfiguration";

export async function getChargePoint(
  location: Location,
  // localorg: LocalOrganisation
  chargerconfig: ChargerConfiguration
) {
  const cp = new ChargePoint();
  cp.chargePointId = faker.random.alphaNumeric(16);
  cp.chargePointName = faker.random.words(3);
  cp.chargePointVendor = faker.random.words(5);
  cp.chargePointSerialNumber = faker.random.numeric(10);
  cp.chargePointModel = faker.random.word();
  cp.ocppProtocol = faker.datatype.number({ max: 10 });
  // cp.lcpOrganisation = localorg;
  // cp.chargingsession = [await createChargingSession(cp)];
  cp.location = location;
  // cp.reviews = [await createReview(cp)];
  cp.modelName = faker.random.word();
  cp.modelSeries = faker.random.alphaNumeric(16);
  cp.modelType = ChargerModelType.BharatAC;
  cp.modelPower = faker.datatype.number({ max: 100 });
  cp.suitableVehicleTypes = faker.datatype.string();
  cp.registractionStatus = RegistrationStatus.Expired;
  cp.chargingStatus = ChargingStatus.Available;
  cp.coordinates = createGeoLocation();
  cp.siteSurveyNumber = faker.datatype.number({ max: 100 });
  cp.siteSurveyDate = faker.datatype.datetime();
  cp.chargerConfiguration = chargerconfig
  cp.activationDate = faker.datatype.datetime();
  // cp.warrantyPeriod = faker.datatype.number();
  cp.warrantyDate = faker.datatype.datetime();
  cp.gunNumber = faker.datatype.number();
  cp.socketNumber = faker.datatype.number();
  cp.sessionOnline = faker.datatype.boolean();
  cp.sessionUpdatedAt = faker.datatype.datetime();
  cp.connectors = [await createConnector(cp)];
  cp.imagesPath = faker.internet.url();
  cp.docPath = faker.internet.url();
  cp.statusUpdatedAt = faker.datatype.datetime();
  cp.cafdocURL = faker.internet.url();
  cp.createdBy = faker.name.firstName();
  cp.updatedBy = faker.name.firstName();
  cp.updatedByUser = faker.name.firstName();
  cp.createdByUser = faker.name.firstName();
  cp.createdAt = faker.date.past();
  // cp.simCardDetails = createSimCardDetails(cp);
  cp.wifiDetails = createWifiDetails(cp);
  // cp.lanDetails = createLANDetails(cp);
  // cp.images = createImage();
  return cp;
}
