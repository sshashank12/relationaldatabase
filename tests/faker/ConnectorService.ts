import { faker } from "@faker-js/faker";
import { ChargePoint } from "../../src/model/ChargePoint";
import { Connector } from "../../src/model/Connector";
import { TariffRateCard } from "../../src/model/TariffRateCard";
import { AppDataSource } from "../../src/repository/Database";
import { getChargePoint } from "./ChargePointService";
import { createConnectorStatus } from "./ConnectorStatusService";
import { createTariff } from "./TariffService";

export async function createConnector(cp: ChargePoint): Promise<Connector> {
  const connector = new Connector();
  connector.chargingStatus = faker.datatype.string();
  connector.connectorStatus = createConnectorStatus(connector, cp);
  connector.chargePoint = cp;
  connector.rateCard = await createTariff();
  return connector;
}
