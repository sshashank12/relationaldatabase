import { faker } from "@faker-js/faker";
import { Transaction, TransactionType } from "../../src/model/Transaction";

export function createTransaction() {
  const transaction = new Transaction();
  transaction.transactionId = faker.random.alphaNumeric(16);
  transaction.transactionType = TransactionType.CHARGING_SESSION;
  transaction.amount = faker.datatype.number();
  transaction.currency = faker.finance.currencyName();
  transaction.state = faker.random.words();
  transaction.coupon = faker.random.words();
  transaction.disputeId = faker.random.alphaNumeric(16);
  // transaction.user = createUserProfile()
  transaction.errorReason = faker.random.words();
  transaction.paymentStatus = faker.random.words();

  return transaction;
}
