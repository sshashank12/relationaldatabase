import { faker } from "@faker-js/faker";
import { TariffRateFlatEntry } from "../../src/model/TariffRateFlatEntry";

export function createTariffRateFlatEntry() {
  const TariffRateFlat = new TariffRateFlatEntry();
  TariffRateFlat.fixedValue = faker.datatype.number();
  TariffRateFlat.perUnitValue = faker.datatype.number();
  TariffRateFlat.startHour = faker.date.past();
  TariffRateFlat.endHour = faker.date.past();
  TariffRateFlat.electricityRate = faker.datatype.number();
  //   TariffRateFlatRateCard
  return TariffRateFlat;
}
