import { faker } from "@faker-js/faker";
import { getOperator } from "./OrganisationService";
import { Organisation } from "../../src/model/Organisation";
import { createLocation } from "./LocationService";
import { Location } from "../../src/model/Location";
import { getLCP } from "./LocalOrganisationService";
import { LocalOrganisation } from "../../src/model/LocalOrganisation";
import { createChargePoint } from "./ChargePointService";
import { ChargePoint } from "../../src/model/ChargePoint";
import { Connector } from "../../src/model/Connector";
import { createConnector } from "./ConnectorService";
import { createChargingSession } from "./ChargingSessionServices";
import { ChargingSession } from "../../src/model/ChargingSession";
import { createConnectorStatus } from "./ConnectorStatusService";
import { ConnectorStatus } from "../../src/model/ConnectorStatus";
import { Address } from "../../src/model/Address";
import { createWifiDetails } from "./WifiDetailsService";
import { WifiDetails } from "../../src/model/WifiDetails";
import { createSimCardDetails } from "./SimCardDetailsService";
import { SimCardDetail } from "../../src/model/SimCardDetails";
import { LANDetails } from "../../src/model/LANDetails";
import { createLANDetails } from "./LANDetailsService";
import { createServiceOrganisation } from "./EmspOrganisationService";
import { ServiceProviderOrganisation } from "../../src/model/ServiceProviderOrganisation";
import { createTariff } from "./TariffService";
import { TariffRateCard } from "../../src/model/TariffRateCard";
import { createReview } from "./ReviewService";
import { Review } from "../../src/model/Review";
import { createMember } from "./UserService";
import { Member } from "../../src/model/Member";
import { AppDataSource } from "../../src/repository/ChargepointRepository";

AppDataSource.initialize()
  .then(async () => {
    // Array.from({ length: 2 }).forEach(() => {
    //
    // });
    createUsers();
    // updateAddress();
  })
  .catch((error) => console.log(error));

async function createUsers() {
  faker.locale = "en_IND";

  for (let i = 0; i < 2; i++) {
    let org = getOperator();
    org = await AppDataSource.getRepository(Organisation).save(org);
    //LCP

    let localorg = getLCP(org);
    localorg = await AppDataSource.getRepository(LocalOrganisation).save(
      localorg
    );
    //CPO

    let serviceorg = createServiceOrganisation();
    serviceorg = await AppDataSource.getRepository(ServiceProviderOrganisation).save(serviceorg);
    //EMSP

    let member = await createMember(org);
    member = await AppDataSource.getRepository(Member).save(member)

    const ratecard = await createTariff(org);
    await AppDataSource.getRepository(TariffRateCard).save(ratecard);

    //4 Locations
    for (let k = 0; k < 2; k++) {
      const loc = await createLocation(localorg, org, ratecard);
      console.log(loc.locationId, "Location ID");
      await AppDataSource.getRepository(Location).save(loc);

      const reviews = await createReview(loc, member)
      await AppDataSource.getRepository(Review).save(reviews)

      // 12 Charge Points

      for (let k = 0; k < 3; k++) {
        const cp = await createChargePoint(loc, localorg, ratecard);
        await AppDataSource.getRepository(ChargePoint).save(cp);

        //12 * 3 = 36 Connectors

        if (k == 1) {
          const wifidetails = await createWifiDetails(cp);
          await AppDataSource.getRepository(WifiDetails).save(wifidetails);
          console.log("Wifi Details has been saved. Id is", wifidetails.id);
        } else if (k == 2) {
          const simdetails = await createSimCardDetails(cp);
          await AppDataSource.getRepository(SimCardDetail).save(simdetails);
        } else {
          const LANdetails = await createLANDetails(cp);
          await AppDataSource.getRepository(LANDetails).save(LANdetails);
        }

        for (let j = 0; j < 3; j++) {
          //Add rate card and pass it to conenctor

          let conn = await createConnector(cp, ratecard);
          await AppDataSource.getRepository(Connector).save(conn);

          const connStatus = await createConnectorStatus(conn, cp);
          await AppDataSource.getRepository(ConnectorStatus).save(connStatus);
          console.log("Connector Status has been saved. Id is", connStatus.id);

          const cs = await createChargingSession(conn, cp, org);
          await AppDataSource.getRepository(ChargingSession).save(cs);
          console.log("Charging Session has been saved. user id is", cs.id);
        }
      }
    }
  }
}
async function updateAddress() {
  await AppDataSource.createQueryBuilder()
    .update(Address)
    .set({ city: "Pune", state: "Maharashtra" })
    .where("id = :id", { id: 1 })
    .execute();
}
