import { faker } from "@faker-js/faker";
import { Revenue } from "../../src/model/Revenue";
export function createRevenue() {
  const revenue = new Revenue();
  revenue.lcp = faker.datatype.number({ min: 0, max: 100 });
  revenue.emsp = faker.datatype.number({ min: 0, max: 100 });

  return revenue;
}
