import { LocalOrganisation } from "../../src/model/LocalOrganisation";
import { faker } from "@faker-js/faker";
import { createContact } from "./ContactService";
import { createAddress } from "./AddressServices";
import { createPayment } from "./PaymentService";
import { createImage } from "./ImageService";
import { OrganisationStatus, OrganisationType } from "../../src/model/Organisation";
import { OperatorOrganisation } from "../../src/model/OperatorOrganisation";

export function getLCP(org: OperatorOrganisation) {
    const localOrganisation = new LocalOrganisation()
    localOrganisation.fullName = faker.name.firstName();
    localOrganisation.shortName = faker.random.word();
    localOrganisation.shortCode = faker.random.alphaNumeric(5);
    localOrganisation.orgtype = OrganisationType.LCP;
    localOrganisation.status = OrganisationStatus.ACTIVE;
    localOrganisation.companyIdNumber = faker.random.numeric();
    localOrganisation.gstNumber = faker.random.numeric();
    localOrganisation.contact = createContact();
    localOrganisation.address = createAddress();
    localOrganisation.paymentDetails = createPayment();
    localOrganisation.website = faker.word.adjective();
    localOrganisation.logo = createImage();
    localOrganisation.agreementDate = faker.datatype.datetime()
    localOrganisation.cpoChargesPerStation = faker.datatype.number()
    localOrganisation.operator = org
    localOrganisation.createdBy = faker.name.firstName();
    localOrganisation.updatedBy = faker.name.firstName();
    localOrganisation.updatedByUser = faker.name.firstName();
    localOrganisation.createdByUser = faker.name.firstName();
    localOrganisation.createdAt = faker.date.past();
    return localOrganisation
}