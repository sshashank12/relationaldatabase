
import { faker } from "@faker-js/faker";
import { Address } from "../../src/model/Address";
faker.locale = "en_IND";

export function createAddress() {
  const address = new Address();
  address.addressLine1 = faker.address.buildingNumber();
  address.addressLine2 = faker.address.secondaryAddress();
  address.landmark = faker.address.cardinalDirection();
  address.what3words = faker.random.words();
  address.zipcode = faker.address.zipCode();
  address.city = faker.address.city();
  address.state = faker.address.state();
  address.country = faker.address.country();
  address.createdBy = faker.name.firstName();
  address.updatedBy = faker.name.firstName();
  address.updatedByUser = faker.name.firstName();
  address.createdByUser = faker.name.firstName();
  address.createdAt = faker.date.past();
  return address;
}
