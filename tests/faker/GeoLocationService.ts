import { GeoLocation } from "../../src/model/GeoLocation";
import { faker } from "@faker-js/faker";

export function createGeoLocation() {
    const geolocation = new GeoLocation()
    geolocation.latitude = faker.random.numeric()
    geolocation.longitude = faker.random.numeric()
    geolocation.createdBy = faker.name.firstName();
    geolocation.updatedByUser = faker.name.firstName();
    geolocation.createdAt = faker.date.past();
    geolocation.monthYear = faker.datatype.string();
    return geolocation
}