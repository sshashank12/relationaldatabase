import { faker } from "@faker-js/faker";
import { Member } from "../../src/model/Member";
import { Organisation } from "../../src/model/Organisation";
import { Access, Role, UserProfile } from "../../src/model/User";
import { createAddress } from "./AddressServices";
import { createFavoriteLocation } from "./FavoriteLocationService";
import { getOperator } from "./OrganisationService";
import { createUserInfo } from "./UserInfoService";
import { createVehicle } from "./VehicleService";

export async function createMember(org: Organisation) {
  const member = new Member();
  member.userId = faker.random.alphaNumeric(16);
  member.email = faker.internet.email();
  member.firstName = faker.name.firstName();
  member.lastName = faker.name.lastName();
  member.dateOfBirth = faker.date.birthdate();
  member.gender = faker.name.gender();
  member.phoneNumber = faker.phone.number();
  member.address = createAddress();
  member.userInfo = createUserInfo();
  member.role = Role.MEMBER;
  member.disabled = faker.datatype.boolean();
  member.passwordChangedDate = faker.date.recent();
  member.access = Access.OPERATOR;
  member.organisation = org;
  // member.favoritelocations = [await createFavoriteLocation(member)]
  member.vehicles = [createVehicle(member)];
  member.createdBy = faker.name.firstName();
  member.updatedBy = faker.name.firstName();
  member.updatedByUser = faker.name.firstName();
  member.createdByUser = faker.name.firstName();
  member.createdAt = faker.date.past();
  return member;
}
