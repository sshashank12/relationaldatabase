import { PaymentDetails, PaymentFrequency } from "../../src/model/Payment";
import { faker } from "@faker-js/faker";

export function createPayment() {
  const payment = new PaymentDetails();
  payment.paymentFrequency = PaymentFrequency.MONTHLY;
  payment.accountNumber = faker.datatype.number();
  payment.accountHolderName = faker.name.firstName();
  payment.bankName = faker.finance.currencyName();
  payment.ifsc = faker.random.alphaNumeric(16);
  payment.createdBy = faker.name.firstName();
  payment.createdAt = faker.date.recent();
  payment.updatedAt = faker.date.recent();
  payment.updatedByUser = faker.name.firstName();
  payment.monthYear = faker.random.word();
  return payment;
}
