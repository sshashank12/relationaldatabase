import { faker } from "@faker-js/faker";
import {
  TariffRateCardEntry,
  TariffFactorTypeEnum,
} from "../../src/model/TariffRateCardEntry";

export function createTariffRateCardEntry() {
  const Tariff = new TariffRateCardEntry();
  Tariff.name = faker.random.words();
  Tariff.type = TariffFactorTypeEnum.FIXED;
  Tariff.value = faker.datatype.number();
  Tariff.comment = faker.lorem.sentence();
  Tariff.startTime = faker.date.past();
  Tariff.isElectricity = faker.datatype.boolean();
  //   tariffRateCard
  return Tariff;
}
