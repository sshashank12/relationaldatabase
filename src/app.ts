import { organisationRouter } from "./routes/OrganisationRouter";
import { locationRouter } from "./routes/LocationRouter";
import { ChargePointRouter } from "./routes/ChargepointRouter";
import { handleError, handleRequest } from "./utils/ResponseWrapper";
import { ChargerConfigRouter } from "./routes/ChargerConfigRouter";

var express = require("express");
export var app = express();
var bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var orgRoute = organisationRouter;
var locRoute = locationRouter;
var cpRoute = ChargePointRouter;
var chargerconfigRouter = ChargerConfigRouter

app.use(handleRequest)
app.use("/organisation", orgRoute);
app.use("/chargepoint", cpRoute);
app.use("/location", locRoute);
app.use("/chargerconfig", chargerconfigRouter)
app.use(handleError);

app.listen(3001, () => {
  console.log("Started on PORT 3000");
});