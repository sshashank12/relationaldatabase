import { DataSource } from "typeorm";

export const AppDataSource = new DataSource({
  type: "postgres",
  host: "localhost",
  port: 5432,
  username: "postgres",
  password: "qwerty123",
  database: "postgres",
  synchronize: true,
  logging: true,
  dropSchema: true,
  // dropSchema: false,
  entities: [__dirname + "/../model/*.{js,ts}"],
  subscribers: [],
  migrations: [],
});
