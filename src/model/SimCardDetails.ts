import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  ChildEntity,
} from "typeorm";
import { ChargePoint } from "./ChargePoint";
import { NetworkDetails } from "./NetworkDetails";

@ChildEntity()
export class SimCardDetail extends NetworkDetails {

  @Column()
  simCardNumber: number;

  @Column({ nullable: true })
  mobileNumber: number;

  @Column({ nullable: true })
  simProvider: string;

  @Column({ nullable: true })
  simType: string;

  @Column({ nullable: true })
  simBillPayer: string;

  @Column({ nullable: true })
  simCardOwner: string;

}
