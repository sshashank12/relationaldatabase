import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { BadgesConditions } from "./BadgesConditions";
import { StaticData } from "./StaticData";

@Entity()
export class Badge extends StaticData {
  @Column()
  badgeTitle: string;

  @Column({ length: 255 })
  badgeDescription: string;

  @Column()
  badgeType: string;

  @Column({ nullable: true })
  expiryDate: Date;

  @Column()
  points: number;

  @Column()
  recurring: boolean;

  @OneToOne(() => BadgesConditions, (badge) => badge.id, { cascade: true })
  @JoinColumn({ name: "conditions" })
  conditions: BadgesConditions;

  @Column({ nullable: true })
  imagePath: string;
}
