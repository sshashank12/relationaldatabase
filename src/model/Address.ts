import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { StaticData } from "./StaticData";

@Entity()
export class Address extends StaticData {
  @Column({ length: 255, comment: "First Line of address", nullable: true })
  addressLine1: string;

  @Column({ length: 255, comment: "Second Line of address", nullable: true })
  addressLine2: string;

  @Column({
    length: 255,
    comment: "Instruction to the landmark",
    nullable: true,
  })
  landmark: string;

  @Column({
    length: 255,
    comment: "what3words to uniquely identify the place",
    nullable: true,
  })
  what3words: string;

  @Column({ comment: "ZIP code of address" })
  zipcode: string;

  @Column({ comment: "City of address" })
  city: string;

  @Column({ comment: "State of address" })
  state: string;

  @Column({ comment: "Country of address" })
  country: string;

  // @Column({ comment: "Search Record of address", nullable: true })
  // searchRecord: string;
  load(data: Address): Address {
    super.load(data)
    this.addressLine1 = data.addressLine1
    this.addressLine2 = data.addressLine2
    this.landmark = data.landmark
    this.what3words = data.what3words
    this.zipcode = data.zipcode
    this.city = data.city
    this.state = data.state
    this.country = data.country
    return this
  } 
}
