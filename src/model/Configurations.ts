import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { Status } from "./ChargingSession";

@Entity()
export class Configurations {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  readonly: boolean;

  @Column()
  key: string;

  @Column()
  currentValue: number;

  @Column()
  defaultValue: number;
}
