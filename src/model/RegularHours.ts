import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";
@Entity()
export class RegularHours {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({
    comment: "Number of day in the week, from Monday (1) till Sunday (7)",
  })
  weekday: number;

  @Column({
    length: 64,
    comment:
      "Begin of the regular period, in local time, given in hours and minutes. Must be in 24h format with leading zeros.",
  })
  period_begin: string;

  @Column({
    length: 64,
    comment:
      "End of the regular period, in local time, syntax as for period_begin. Must be later than period_begin.",
  })
  period_end: string;
}
