import { Min } from "class-validator";
import { ChildEntity, Column, JoinColumn, OneToOne, OneToMany } from "typeorm";
import { Location } from "./Location";
import { OperatorOrganisation } from "./OperatorOrganisation";
import { Organisation } from "./Organisation";

@ChildEntity()
export class LocalOrganisation extends Organisation {
  @Column({
    nullable: true,
    comment: "Agreement Date",
  })
  agreementDate: Date;

  @OneToMany(() => Location, (Location) => Location.id)
  location: Location[];

  @Column({ nullable: true })
  @Min(0)
  cpoChargesPerStation: number;

  @OneToOne(() => OperatorOrganisation, { cascade: false, lazy: true })
  @JoinColumn()
  operator: OperatorOrganisation;

  load(data: LocalOrganisation):LocalOrganisation {
    super.load(data)
    this.fullName = data.fullName
    this.shortName = data.shortName
    this.shortCode = data.shortCode
    this.orgtype = data.orgtype
    this.status = data.status
    this.companyIdNumber = data.companyIdNumber
    this.gstNumber = data.gstNumber
    this.contact = data.contact
    this.address = data.address
    this.paymentDetails = data.paymentDetails
    this.website = data.website
    this.logo = data.logo
    this.agreementDate = data.agreementDate
    this.location = data.location
    this.cpoChargesPerStation = data.cpoChargesPerStation
    this.operator = data.operator
    return this
  }
}
