import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { StaticData } from "./StaticData";

@Entity()
export class CompanyInfo extends StaticData {
  @Column()
  companyName: string;

  @Column()
  companyLogo: string;

  @Column()
  companyGstin: string;

  @Column()
  companyCin: string;

  @Column()
  companyAddress: string;

  @Column()
  companyId: string;

  @Column()
  companyContact: string;
}
