import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Address } from "./Address";
import { StaticData } from "./StaticData";
import { TrackingDetails } from "./TrackingDetails";

export enum CardRequestStatus {
  OPEN = "Open",
  INPROGRESS = "InProgress",
  DISPATCHED = "Dispatched",
  COMPLETED = "Completed",
  REJECTED = "Rejected",
}

export enum CardRequestType {
  NEW = "New",
  ACTIVATE = "Activate",
  BLOCK = "Block",
  UNBLOCK = "Unblock",
}

export enum CardType {
  PHYSICAL = "Physical",
  VIRTUAL = "Virtual",
}

@Entity()
export class CardRequest extends StaticData {
  @Column()
  requestId: string;

  @Column()
  cardNumber: string;

  @Column({ type: "enum", enum: CardRequestStatus })
  status: CardRequestStatus;

  @Column()
  userId: string;

  @Column()
  phoneNumber: string;

  @OneToOne(() => Address, (address) => address.id)
  @JoinColumn()
  address: Address;

  @Column({ type: "enum", enum: CardRequestStatus })
  type: CardRequestStatus;

  @Column({ type: "enum", enum: CardType })
  cardType: CardType;

  @Column()
  description: string;

  @Column()
  nextActionOwner: string;

  @OneToOne(() => TrackingDetails, (trackingdetails) => trackingdetails.id)
  @JoinColumn()
  trackingDetails: TrackingDetails;

  @Column()
  amtCharged: Number;
}
