import { Min } from "class-validator";
import { ChildEntity, Column } from "typeorm";
import { Organisation } from "./Organisation";

@ChildEntity()
export class ServiceProviderOrganisation extends Organisation {
  @Column()
  @Min(0)
  taxRate: number;

  load(data: ServiceProviderOrganisation): ServiceProviderOrganisation{
    super.load(data)
    this.fullName = data.fullName
    this.shortName = data.shortName
    this.shortCode = data.shortCode
    this.orgtype = data.orgtype
    this.status = data.status
    this.companyIdNumber = data.companyIdNumber
    this.gstNumber = data.gstNumber
    this.contact = data.contact
    this.address = data.address
    this.paymentDetails = data.paymentDetails
    this.website = data.website
    this.logo = data.logo
    this.taxRate = data.taxRate
    return this
  }
}
