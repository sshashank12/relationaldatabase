import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { TransactionalData } from "./TransactionData";

@Entity()
export class GeoLocation extends TransactionalData {
  @Column({
    length: 10,
    comment:
      'Latitude of the point in decimal degree. Example: 50.770774. Decimal separator: "." Regex: -?[0-9]{1,2}.[0-9]{5,7}',
  })
  latitude: string;

  @Column({
    length: 11,
    comment:
      'Longitude of the point in decimal degree. Example: -126.104965. Decimal separator: "." Regex: -?[0-9]{1,3}.[0-9]{5,7}',
  })
  longitude: string;
}
