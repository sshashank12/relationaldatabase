import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Configurations } from "./Configurations";
import { StaticData } from "./StaticData";

export enum Status {
  Active = "Active",
  Inactive = "Inactive",
}

export enum ChargerModelType {
  Type2 = "Type2",
  BharatAC = "BharatAC",
}

@Entity()
export class ChargerConfiguration extends StaticData {

  @Column()
  configName: string;

  @Column()
  controller: string;

  @Column({ type: "enum", enum: Status })
  status: Status;

  @Column()
  firmwareVersion: number;

  @Column()
  isDefault: boolean;

  @Column({ type: "enum", enum: ChargerModelType })
  chargerType: ChargerModelType;

  @OneToOne(() => Configurations, (configuration) => configuration.id)
  @JoinColumn()
  configuration: Configurations;

  load(data: ChargerConfiguration): ChargerConfiguration {
    super.load(data)
    this.configName = data.configName
    this.controller = data.controller
    this.status = data.status
    this.firmwareVersion = data.firmwareVersion
    this.isDefault = data.isDefault
    this.chargerType = data.chargerType
    this.configuration = data.configuration
    return this
  }
}
