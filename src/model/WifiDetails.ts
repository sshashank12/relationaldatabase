import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  ChildEntity,
} from "typeorm";
import { ChargePoint } from "./ChargePoint";
import { NetworkDetails } from "./NetworkDetails";

@ChildEntity()
export class WifiDetails extends NetworkDetails {
  @Column({ nullable: true })
  wifiSSID: string;

  @Column({ nullable: true })
  wifiProvider: string;

  @Column({ nullable: true })
  wifiPassword: string;

  @Column({ nullable: true })
  wifiAuthenticationType: string;

  @Column({ nullable: true })
  macAddress: number;
}
