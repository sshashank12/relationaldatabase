import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  TableInheritance,
  UpdateDateColumn,
} from "typeorm";

export class StaticData {
  @PrimaryGeneratedColumn()
  id: string

  @Column()
  createdBy: string;

  @CreateDateColumn()
  createdAt: Date;

  @Column()
  updatedBy: string;

  @UpdateDateColumn()
  updatedAt: Date;

  @Column()
  updatedByUser: string;

  @Column({ nullable: true })
  createdByUser: string;

  load(data: StaticData): StaticData {
    this.id = data.id
    this.createdBy = data.createdBy
    this.createdAt = data.createdAt
    this.updatedBy = data.updatedBy
    this.updatedAt = data.updatedAt
    this.updatedByUser = data.updatedByUser
    this.createdByUser = data.createdByUser
    return this
  }
}
