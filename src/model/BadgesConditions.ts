import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class BadgesConditions {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ nullable: true })
  description: string;

  @Column()
  title: string;

  @Column({ nullable: true })
  fieldName: string;

  @Column()
  operator: string;

  @Column()
  value: string;
}
