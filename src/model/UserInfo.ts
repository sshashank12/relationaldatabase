import { ChildEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { StaticData } from "./StaticData";

@Entity()
export class UserInfo extends StaticData {

  @Column()
  emailVerified: boolean;

  @Column({ nullable: true })
  role: string;

  @Column({ nullable: true })
  access: string;

  load(data: UserInfo): UserInfo{
    super.load(data)
    this.emailVerified = data.emailVerified
    this.role = data.role
    this.access = data.access
    return this
  }
}
