import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";
import { StaticData } from "./StaticData";

export enum ImageCategory {
  CHARGER = "Charger",
  ENTRANCE = "Entrance",
  LOCATION = "Location",
  NETWORK = "Network",
  OPERATOR = "Operator",
  OTHER = "Other",
  OWNER = "Owner",
}

@Entity()
export class Image extends StaticData {
  @Column({
    length: 255,
    comment:
      "URL from where the image data can be fetched through a web browser",
  })
  url: string;

  @Column({
    length: 255,
    nullable: true,
    comment:
      "URL from where a thumbnail of the image can be fetched through a webbrowser.",
  })
  thumbnail: string;

  @Column({
    type: "enum",
    enum: ImageCategory,
    comment: "Describes what the image is used for.",
  })
  category: ImageCategory;

  @Column({
    length: 255,
    nullable: true,
    comment: "Length of the full scale image.",
  })
  type: string;

  @Column({ nullable: true, comment: "Width of the full scale image." })
  width: number;

  @Column({ nullable: true, comment: "Height of the full scale image." })
  height: number;
}
