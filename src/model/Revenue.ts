import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { StaticData } from "./StaticData";

@Entity()
export class Revenue extends StaticData {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  lcp: number;

  @Column()
  emsp: number;
}
