import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { StaticData } from "./StaticData";

@Entity()
export class Contact extends StaticData {
  @Column({ length: 255, comment: "Begin of the scheduled period." })
  name: string;

  @Column({ comment: "End of the scheduled period, if known." })
  phoneNumber: string;

  @Column({
    nullable: true,
    length: 255,
    comment: "End of the scheduled period, if known.",
  })
  email: string;

  @Column({
    nullable: true,
    length: 255,
    comment: "End of the scheduled period, if known.",
  })
  designation: string;
}
