import {
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  TableInheritance,
} from "typeorm";
import { ChargePoint } from "./ChargePoint";
import { StaticData } from "./StaticData";

@Entity()
@TableInheritance({ column: { type: "varchar", name: "type" } })
export class NetworkDetails extends StaticData {
  @PrimaryGeneratedColumn()
  id: string;

  @OneToOne(() => ChargePoint, (scd) => scd.id, {
    cascade: false,
    eager: true,
  })
  @JoinColumn()
  chargePoint: ChargePoint;
}
