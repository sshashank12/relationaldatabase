import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { StaticData } from "./StaticData";
import { UserProfile } from "./User";

@Entity()
export class UserStats extends StaticData {
  @Column({ nullable: true })
  co2Saved: number;

  @Column({ nullable: true })
  chargingSessionCount: number;

  @Column({ nullable: true })
  kmsDriven: number;

  @Column({ nullable: true })
  moneySaved: number;

  @Column({ nullable: true })
  freeKMCount: number;

  @Column({ nullable: true })
  referralCount: number;

  @OneToOne(() => UserProfile, (userprofile) => userprofile.id)
  @JoinColumn()
  user: UserProfile;
}
