import { Column, ChildEntity } from "typeorm";
import { NetworkDetails } from "./NetworkDetails";

@ChildEntity()
export class LANDetails extends NetworkDetails {
  @Column({ nullable: true })
  macAddress: number;
}
