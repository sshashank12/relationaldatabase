import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToOne,
  JoinColumn,
  OneToMany,
} from "typeorm";
import { ChargePoint } from "./ChargePoint";
import { Connector } from "./Connector";
import { ContractInfo } from "./ContractInfo";
import { DisputeResolution } from "./DisputeResolution";
import { IdTag } from "./IdTag";
import { Location } from "./Location";
import { Revenue } from "./Revenue";
import { TariffRateCard } from "./TariffRateCard";
import { Transaction } from "./Transaction";
import { TransactionalData } from "./TransactionData";

export enum Status {
  ACTIVE = "Active",
  INACTIVE = "Inactive",
}

@Entity()
export class ChargingSession extends TransactionalData {
  @ManyToOne(() => ChargePoint, (chargepoint) => chargepoint.id)
  chargePoint: ChargePoint;

  @Column()
  connectorId: string;

  @Column()
  taxableValue: string;

  @OneToOne(() => IdTag, (idtag) => idtag.id)
  idtag: IdTag;

  @Column()
  lastUpdatedAt: Date;

  @Column()
  startedAt: Date;

  @Column()
  lastValue: number;

  @Column()
  startValue: number;

  @Column({
    type: "enum",
    enum: Status,
    comment: "Status of the charging session",
  })
  status: Status; //enum

  @Column()
  lastTime: Date;

  @Column()
  stopReason: string; //enum

  @Column()
  stopReasonDetail: string;

  @OneToOne(() => Transaction, (transaction) => transaction.id)
  @JoinColumn()
  transaction: Transaction;

  @Column()
  costRefund: number;

  @OneToMany(() => TariffRateCard, (tariffratecard) => tariffratecard.id)
  tariffRateCard: TariffRateCard[];

  @Column()
  cost: number;

  @Column()
  cutOffAmount: number;

  @Column()
  connectionFee: number;

  @Column()
  finalFixedRate: number;

  @Column()
  finalPerUnitRate: number;

  @Column()
  taxRate: number;

  @Column()
  rateUnit: string; //enum

  @Column()
  rateName: string;

  @Column()
  rateId: string;

  @Column()
  sessionCostCashUsed: number;

  @Column()
  sessionCostCreditUsed: number;

  @Column()
  transactionDocId: string;

  @Column()
  adjustmentCost: number;

  @Column()
  userWalletId: string;

  @OneToMany(
    () => DisputeResolution,
    (disputeresolution) => disputeresolution.id
  )
  disputeResolution: DisputeResolution[];

  @Column()
  disputeRaised: boolean;

  @OneToOne(() => ContractInfo, (contractinfo) => contractinfo.id)
  @JoinColumn()
  contractInfo: ContractInfo;

  @Column()
  revenueSharedPerUnit: number;

  @Column()
  electricityCost: number;

  @Column()
  revenueSharedFixed: number;

  @OneToOne(() => Revenue, (revenue) => revenue.id)
  @JoinColumn()
  revenue: Revenue;

  @Column()
  phoneNumber: string;

  @Column()
  chargePointModel: string;

  @Column()
  deductionAmount: number;

  @Column()
  invoiceStatus: string;

  @OneToOne(() => Connector, { lazy: true })
  @JoinColumn()
  connector: Connector;
}
