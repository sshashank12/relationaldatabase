import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { StaticData } from "./StaticData";
import { UserProfile } from "./User";

@Entity()
export class Vehicle extends StaticData {
  @PrimaryGeneratedColumn()
  id: string;

  @ManyToOne(() => UserProfile, (userprofile) => userprofile.id)
  user: UserProfile;

  @Column({ nullable: true })
  licensePlate: string;

  @Column()
  isActive: boolean;

  @Column({ nullable: true })
  batteryCapacity: number;

  @Column({ nullable: true })
  batterySwappable: boolean;

  @Column({ nullable: true })
  bodyType: string;

  @Column({ nullable: true })
  chargingTime: string;

  @Column()
  connectorType: string;

  @Column()
  connectorTypesSupported: string;

  @Column({ nullable: true })
  imagesPath: string;

  @Column()
  manufacturer: string;

  @Column()
  model: string;

  @Column({ nullable: true })
  range: number;

  @Column({ nullable: true })
  variant: string;

  @Column({ nullable: true })
  vehicleId: string;

  @Column({ nullable: true })
  wheelerType: string;

  @Column()
  vehicleType: string;

  @Column({ nullable: true })
  fastChargingSupported: boolean;
}
