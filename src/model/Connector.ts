import {
  Column,
  Entity,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
  JoinColumn,
} from "typeorm";
import { ChargePoint } from "./ChargePoint";
import { ChargingSession } from "./ChargingSession";
import { ConnectorStatus } from "./ConnectorStatus";
import { TariffRateCard } from "./TariffRateCard";

@Entity()
export class Connector {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  chargingStatus: string;

  @OneToOne(() => ConnectorStatus, (connectorstatus) => connectorstatus.id)
  connectorStatus: ConnectorStatus;

  @OneToOne(() => TariffRateCard, (tariffRateCard) => tariffRateCard.connector)
  @JoinColumn()
  rateCard: TariffRateCard;

  @ManyToOne(() => ChargePoint, (cp) => cp.id)
  chargePoint: ChargePoint;

}
