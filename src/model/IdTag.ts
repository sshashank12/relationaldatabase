import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Member } from "./Member";
import { StaticData } from "./StaticData";

export enum IdTagStatus {
  UNASSIGNED = "Unassigned",
  ACTIVE = "Active",
  INPROGRESS = "InProgress",
  DISPATCHED = "Dispatched",
  INACTIVE = "Inactive",
  BLOCKED = "Blocked",
  EXPIRED = "Expired",
}

export enum CardType {
  PHYSICAL = "Physical",
  VIRTUAL = "Virtual",
}

@Entity()
export class IdTag extends StaticData {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  cardNumber: string;

  @Column()
  rfid: string;

  @Column({ type: "enum", enum: IdTagStatus })
  status: IdTagStatus;

  @Column({ type: "enum", enum: CardType })
  type: CardType;

  @ManyToOne(() => Member, (userprofile) => userprofile.id)
  user: Member;

  @Column({ nullable: true })
  batchId: string;

  @Column()
  issueDate: Date;
}
