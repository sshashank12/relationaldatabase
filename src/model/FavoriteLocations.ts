import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  ManyToOne,
} from "typeorm";
import { Connector } from "./Connector";
import { Location } from "./Location";
import { Member } from "./Member";
import { TransactionalData } from "./TransactionData";
import { UserProfile } from "./User";

@Entity()
export class FavoriteLocations extends TransactionalData {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  name: string;

  @ManyToOne(() => Member, (userprofile) => userprofile.id)
  user: Member;

  @OneToOne(() => Location, { eager: true, cascade: false })
  location: Location;
}
