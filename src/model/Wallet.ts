import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { TransactionalData } from "./TransactionData";
import { UserProfile } from "./User";

@Entity()
export class Wallet extends TransactionalData {
  @Column()
  balance: number;

  @OneToOne(() => UserProfile, (userprofile) => userprofile.id)
  @JoinColumn()
  user: UserProfile;

  @Column()
  userName: string;

  @Column()
  phoneNumber: string;
}
