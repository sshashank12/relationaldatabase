import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  VersionColumn,
  JoinColumn,
} from "typeorm";
import { ChargePoint } from "./ChargePoint";
import { ChargingSession } from "./ChargingSession";
import { Connector } from "./Connector";
import { Organisation } from "./Organisation";
import { TariffRateCardEntry } from "./TariffRateCardEntry";
import { TariffRateFlatEntry } from "./TariffRateFlatEntry";

export enum TariffTypeEnum {
  PER_KWH = "per kWh",
}

@Entity()
export class TariffRateCard {
  @PrimaryGeneratedColumn()
  id: string;

  @ManyToOne(() => Organisation, (organisation) => organisation.id)
  organisation: Organisation;

  @Column({ type: "enum", enum: TariffTypeEnum })
  rateUnit: TariffTypeEnum;

  @Column()
  name: string;

  @OneToOne(() => Connector, (scd) => scd.rateCard)
  @JoinColumn()
  connector: Connector;

  @VersionColumn()
  version: number;
  //add version to all non transaction
  // create object static data(data,), transaction data and extend them ()

  @OneToMany(
    () => TariffRateCardEntry,
    (tariffratecardentry) => tariffratecardentry.id
  )
  entries: TariffRateCardEntry[];

  @OneToMany(
    () => TariffRateFlatEntry,
    (tariffrateflatentry) => tariffrateflatentry.id
  )
  rateEntries: TariffRateFlatEntry[];

  @Column()
  finalFixedRate: number;

  @Column()
  finalPerUnitRate: number;

  @Column({ nullable: true })
  electricityProvider: string;

  @Column({ nullable: true })
  consumerNumber: number;

  @ManyToOne(() => ChargingSession, (chargingsession) => chargingsession.id)
  chargingSession: ChargingSession;
}
