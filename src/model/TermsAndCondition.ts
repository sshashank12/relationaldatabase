import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { StaticData } from "./StaticData";

@Entity()
export class TermsAndCondition extends StaticData {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  version: number;

  @Column()
  acceptedOn: Date;
}
