import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { StaticData } from "./StaticData";
import { UserProfile } from "./User";

export enum CouponState {
  UNASSIGNED = "Unassigned",
  ASSIGNED = "Assigned",
  RESERVED = "Reserved",
  EXPIRED = "Expired",
}

@Entity()
export class Coupons extends StaticData {
  @Column({ length: 48 })
  code: string;

  @Column()
  amount: number;

  @Column({ type: "enum", enum: CouponState })
  couponState: CouponState;

  @Column({ nullable: true })
  expiryDate: Date;

  @OneToOne(() => UserProfile, (userprofile) => userprofile.id)
  @JoinColumn()
  user: UserProfile;

  @Column({ nullable: true })
  reservedByUserId: string;

  @Column({ nullable: true })
  reservedByUserName: string;

  @Column({ nullable: true })
  reservedDesctiption: string;

  @Column({ nullable: true })
  reserved: boolean;

  @Column({ nullable: true })
  footer: string;

  @Column({ length: 255 })
  description: string;
}
