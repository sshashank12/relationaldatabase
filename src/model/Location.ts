import {
  Entity,
  Column,
  OneToOne,
  JoinColumn,
  OneToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { GeoLocation } from "./GeoLocation";
import { ChargePoint } from "./ChargePoint";
import { OperatorOrganisation } from "./OperatorOrganisation";
import { LocalOrganisation } from "./LocalOrganisation";
import { Address } from "./Address";
import { ChargingSession } from "./ChargingSession";
import { ContractInfo } from "./ContractInfo";
import { Contact } from "./Contact";
import { StaticData } from "./StaticData";
import { Organisation } from "./Organisation";
import { Review } from "./Review";

export enum TokenType {
  AD_HOC_USER = "AD_HOC_USER",
  APP_USER = "APP_USER",
  OTHER = "OTHER",
  RFID = "RFID",
}

export enum LocationStatus {
  PENDING = "Pending",
  ACTIVE = "Active",
  RETIRED = "Retired",
  BLOCKED = "Blocked",
}

export enum ParkingType {
  ALONG_MOTORWAY = "ALONG_MOTORWAY",
  PARKING_GARAGE = "PARKING_GARAGE",
  PARKING_LOT = "PARKING_LOT",
  ON_DRIVEWAY = "ON_DRIVEWAY",
  ON_STREET = "ON_STREET",
  UNDERGROUND_GARAGE = "UNDERGROUND_GARAGE",
}

export enum LocationType {
  PUBLIC = "PUBLIC",
  CAPTIVE = "CAPTIVE",
  RESTRICTED = "RESTRICTED",
}

export enum Facility {
  HOTEL = "HOTEL",
  PARKING_GARAGE = "RESTAURANT",
  CAFE = "CAFE",
  MALL = "MALL",
  SUPERMARKET = "SUPERMARKET",
  RECREATION_AREA = "RECREATION_AREA",
  NATURE = "NATURE",
  MUSEUM = "MUSEUM",
  BIKE_SHARING = "BIKE_SHARING",
  BUS_STOP = "BUS_STOP",
  TAXI_STAND = "Taxi Stand",
  TRAM_STOP = "TRAM_STOP",
  METRO_STATION = "METRO_STATION",
  TRAIN_STATION = "TRAIN_STATION",
  AIRPORT = "AIRPORT",
  PARKING_LOT = "PARKING_LOT",
  CARPOOL_PARKING = "CARPOOL_PARKING",
  FUEL_STATION = "FUEL_STATION",
  WIFI = "WIFI",
}
@Entity()
export class Location extends StaticData {
  @Column({
    unique: true,
  })
  docId: string; // This is unqie

  @Column({
    length: 36,
    comment: "This indicaqtes the chargepoint model",
    unique: true,
  })
  locationId: string;

  @Column({
    length: 36,
    unique: true,
    comment: "Display short name of the location.",
  })
  shortName: string;

  @Column({
    length: 5,
    unique: true,
  })
  shortCode: string;

  @Column()
  showOnMap: boolean;

  @Column({ length: 255, comment: "Display name of the location." })
  name: string;

  @OneToOne(() => Address, { cascade: true, lazy: true })
  @JoinColumn()
  address: Address;

  @OneToOne(() => GeoLocation, { cascade: true, eager: true })
  @JoinColumn()
  coordinates: GeoLocation;

  @Column({
    type: "enum",
    enum: ParkingType,
    comment: "The general type of parking at the charge point location.",
  })
  parkingType: ParkingType;

  @OneToMany(() => ChargePoint, (cp) => cp.id)
  chargepoints: ChargePoint[];

  @ManyToOne(
    () => OperatorOrganisation,
    (operatororganisation) => operatororganisation.id
  )
  operator: OperatorOrganisation;

  @ManyToOne(
    () => OperatorOrganisation,
    (operatororganisation) => operatororganisation.id
  )
  suboperator: OperatorOrganisation;

  @ManyToOne(
    () => LocalOrganisation,
    (localorganisation) => localorganisation.id
  )
  owner: LocalOrganisation;

  // Organisation DocId?? And operationOrganisationDocID?? Need to figure out
  @OneToOne(() => Contact, { cascade: true, lazy: true })
  @JoinColumn()
  contact: Contact;

  @OneToOne(() => ContractInfo, { cascade: true, lazy: true })
  @JoinColumn()
  contractInfo: ContractInfo;

  @Column({
    type: "enum",
    enum: Facility,
    array: true,
    comment:
      "Optional list of facilities this charging location directly belongs to.",
  })
  amenities: Facility[];

  @Column({
    type: "enum",
    enum: LocationType,
    comment:
      "Optional list of facilities this charging location directly belongs to.",
  })
  type: LocationType; //Enum of location type //Public,captive and restricted

  @Column({
    type: "enum",
    enum: LocationStatus,
    comment: "Status of the Location.",
  })
  status: LocationStatus;

  //location tag name of table. Colummn are

  @Column({
    comment:
      "Timestamp when this Location or one of its EVSEs or Connectors were last updated (or created).",
  })
  last_updated: Date;

  @OneToMany(() => Review, rev => rev.location)
  reviews: Review[];

  load(data: Location): Location {
    super.load(data)
    this.docId = data.docId
    this.locationId = data.locationId
    this.shortName = data.shortName
    this.shortCode = data.shortCode
    this.showOnMap = data.showOnMap
    this.name = data.name
    this.address = data.address
    this.coordinates = data.coordinates
    this.parkingType = data.parkingType
    this.contact = data.contact
    this.address = data.address
    this.chargepoints = data.chargepoints
    this.operator = data.operator
    this.suboperator = data.suboperator
    this.owner = data.owner
    this.contact = data.contact
    this.contractInfo = data.contractInfo
    this.amenities = data.amenities
    this.type = data.type
    this.status = data.status
    this.last_updated = data.last_updated
    this.reviews = data.reviews
    return this
  }

}
