import { IsFQDN } from "class-validator";
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  TableInheritance,
  OneToMany,
} from "typeorm";
import { Address } from "./Address";
import { Contact } from "./Contact";
import { Image } from "./Image";
import { Location } from "./Location";
import { PaymentDetails } from "./Payment";
import { StaticData } from "./StaticData";

export enum OrganisationType {
  EMSP = "EMSP",
  LCP = "LCP",
  CPO = "CPO",
}

export enum OrganisationStatus {
  PENDING = "Pending",
  ACTIVE = "Active",
  RETIRED = "Retired",
  BLOCKED = "Blocked",
}

@Entity()
@TableInheritance({ column: { type: "varchar", name: "type" } })

export class Organisation extends StaticData {
  @Column({
    length: 255,
    comment: "OCPI Field - Name of the organisation.",
  })
  fullName: string;

  @Column({
    length: 32,
    comment: "Name of the organisation.",
  })
  shortName: string;

  @Column({
    length: 5,
    comment: "Name of the organisation.",
    unique: true,
  })
  shortCode: string;

  @Column({
    type: "enum",
    enum: OrganisationType,
    comment: "Type of the organisation.",
  })
  orgtype: OrganisationType;

  @Column({
    type: "enum",
    enum: OrganisationStatus,
    comment: "Status of the organisation.",
  })
  status: OrganisationStatus;

  @Column({
    nullable: true,
    length: 100,
    comment: "Company Identification Number",
  })
  companyIdNumber: string;

  @Column({
    nullable: true,
    length: 255,
    comment: "GST Number",
  })
  gstNumber: string;

  @OneToOne(() => Contact, { cascade: true, lazy: true })
  @JoinColumn()
  contact: Contact;

  @OneToOne(() => Address, { cascade: true, lazy: true })
  @JoinColumn()
  address: Address;

  @OneToOne(() => PaymentDetails, { cascade: true, lazy: true })
  @JoinColumn()
  paymentDetails: PaymentDetails;

  @Column({
    length: 255,
    comment: "OCPI Field - Link to the organisation's website",
    nullable: true,
  })
  @IsFQDN()
  website: string;

  @OneToOne(() => Image, { cascade: true, lazy: true })
  @JoinColumn()
  logo: Image;
 
  load(data: Organisation): Organisation {
    super.load(data)
    this.fullName = data.fullName
    this.shortName = data.shortName
    this.shortCode = data.shortCode
    this.orgtype = data.orgtype
    this.status = data.status
    this.companyIdNumber = data.companyIdNumber
    this.gstNumber = data.gstNumber
    this.contact = data.contact
    this.address = data.address
    this.paymentDetails = data.paymentDetails
    this.website = data.website
    this.logo = data.logo
    return this
  }
}
