import {
  ChildEntity,
  OneToMany,
  OneToOne,
  JoinColumn,
  Entity,
  TableInheritance,
} from "typeorm";
import { TermsAndCondition } from "./TermsAndCondition";
import { UserProfile } from "./User";
import { Wallet } from "./Wallet";
import { CaptiveMember } from "./CaptiveMember";
import { IdTag } from "./IdTag";
import { StaticData } from "./StaticData";
import { FavoriteLocations } from "./FavoriteLocations";
import { Vehicle } from "./Vehicle";
import { Review } from "./Review";

@ChildEntity()
export class Member extends UserProfile {
  @OneToOne(() => Wallet, (wallet) => wallet.id)
  @JoinColumn()
  wallet: Wallet;

  @OneToOne(
    () => TermsAndCondition,
    (termsandcondition) => termsandcondition.id
  )
  @JoinColumn()
  termsAndCondition: TermsAndCondition;

  @OneToMany(() => CaptiveMember, (captivemember) => captivemember.id)
  captivemembers: CaptiveMember[];

  @OneToMany(() => IdTag, (idtag) => idtag.id)
  idtags: IdTag[];

  @OneToMany(
    () => FavoriteLocations,
    (favoritelocation) => favoritelocation.id,
    { cascade: true, lazy: true }
  )
  favoritelocations: FavoriteLocations[];

  @OneToMany(() => Vehicle, (vehicle) => vehicle.id, { cascade: true })
  vehicles: Vehicle[];

  @OneToMany(() => Review, (review) => (review.user))
  reviews: Review[]
}
