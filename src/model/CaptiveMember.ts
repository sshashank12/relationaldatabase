import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Location } from "./Location";
import { UserProfile } from "./User";

export enum CaptiveMemberStatus {
  PENDING = "Pending",
  ACTIVE = "Active",
  BLOCKED = "Blocked",
}

@Entity()
export class CaptiveMember {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ type: "enum", enum: CaptiveMemberStatus })
  status: string;

  @OneToOne(() => Location, (location) => location.id, {
    cascade: false,
    lazy: true,
  })
  @JoinColumn()
  location: Location;

  @ManyToOne(() => UserProfile, (userprofile) => userprofile.id)
  user: UserProfile;
}
