import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  OneToMany,
  TableInheritance,
  ChildEntity,
} from "typeorm";
import { Address } from "./Address";
import { FavoriteLocations } from "./FavoriteLocations";
import { Organisation } from "./Organisation";
import { Review } from "./Review";
import { StaticData } from "./StaticData";
import { UserInfo } from "./UserInfo";
import { Vehicle } from "./Vehicle";

export enum Access {
  ADMIN = "Admin", // Super Admin
  OPERATOR = "Operator", //Portal Operator
  MEMBER = "Member", //Mobile app access
}

export enum Role {
  ADMIN = "Admin", // Super Admin
  OPERATOR = "Operator", //Portal Operator
  MEMBER = "Member", //EV driver as Member
}

export enum OrganisationType {
  CPO = "CPO", //Charge Point Operator
  LCP = "LCP", //Local Chargepoint Operator
  EMSP = "EMSP", //eMobility Service Provider
}

@Entity()
@TableInheritance({ column: { type: "varchar", name: "type" } })
export class UserProfile extends StaticData {
  @Column()
  userId: string;

  @Column()
  email: string;

  @Column({ nullable: true })
  firstName: string;

  @Column({ nullable: true })
  lastName: string;

  @Column()
  dateOfBirth: Date;

  @Column({ nullable: true })
  gender: string;

  @Column({ nullable: true })
  phoneNumber: string;

  @OneToOne(() => Address, (address) => address.id, {
    cascade: false,
    lazy: true,
  })
  @JoinColumn()
  address: Address;

  @OneToOne(() => UserInfo, (userInfo) => userInfo.id)
  @JoinColumn()
  userInfo: UserInfo;

  @Column({ type: "enum", enum: Role })
  role: Role;

  @Column()
  disabled: boolean;

  @Column()
  passwordChangedDate: Date;

  @Column({ type: "enum", enum: Access })
  access: Access;

  @OneToOne(() => Organisation, (organisation) => organisation.id)
  @JoinColumn()
  organisation: Organisation;

}
