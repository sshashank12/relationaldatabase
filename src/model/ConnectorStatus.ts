import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { Connector } from "./Connector";
import { ChargePoint } from "./ChargePoint";

export enum ChargePointErrorCode {
  AD_HOC_USER = "AD_HOC_USER",
  APP_USER = "APP_USER",
  OTHER = "OTHER",
  RFID = "RFID",
}

export enum Status {
  AVAILABLE = "Available",
  BLOCKED = "Blocked",
  CHARGING = "Charging",
  INOPERATIVE = "Inoperative",
  OUTOFORDER = "Outoforder",
  PLANNED = "Planned",
  REMOVED = "Removed",
  RESERVED = "Reserved",
  UNKNOWN = "Unknown",
}

@Entity()
export class ConnectorStatus {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => Connector, { lazy: true })
  @JoinColumn()
  connector: Connector;

  @ManyToOne(() => ChargePoint, (evse) => evse.chargePointId, { lazy: true })
  @JoinColumn({ name: "chargePointId" })
  evse: ChargePoint;

  @Column({
    type: "enum",
    enum: ChargePointErrorCode,
    comment: "This contains the error code reported by the Charge Point.",
  })
  errorCode: ChargePointErrorCode;

  @Column({ length: 50, comment: "General Connector Information" })
  info: string;

  @Column({
    type: "enum",
    enum: Status,
    comment: "This indicates the success or failure of the data transfer",
  })
  status: Status;

  @Column({ comment: "Timestamp for measured value(s)" })
  timestamp: Date;

  @Column({
    nullable: true,
    length: 255,
    comment: "This contains the vendor-specific error code.",
  })
  vendorErrorCode: string;

  @Column({ length: 255, comment: "End of the scheduled period, if known." })
  vendorId: string;
}
