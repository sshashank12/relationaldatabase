import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { TransactionalData } from "./TransactionData";
import { UserProfile } from "./User";

export enum TransactionType {
  TOPUP = "TU",
  COUPON_CREDIT = "CC",
  COUPON_EXPIRED = "CE",
  CHARGING_SESSION = "CS",
  CHARGE_CARD_TRANSACTION = "CCT",
  CHARGE_CARD_ROLLBACK = "CCR",
  CHARGING_SESSION_ROLLBACK = "CSR",
  WALLET_BALANCE_ROLLBACK = "WBR",
  WALLET_BALANCE_ADUJSTMENT = "WBA",
}

@Entity()
export class Transaction extends TransactionalData {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  transactionId: string;

  @Column({ type: "enum", enum: TransactionType })
  transactionType: TransactionType;

  @Column()
  amount: number;

  @Column()
  currency: string;

  @Column()
  state: string;

  @Column()
  coupon: string;

  @Column()
  disputeId: string;

  @OneToOne(() => UserProfile, (userprofile) => userprofile.id)
  @JoinColumn()
  user: UserProfile;

  @Column()
  errorReason: string;

  @Column()
  paymentStatus: string;
}
