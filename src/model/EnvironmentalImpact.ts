import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";
export enum EnvironmentalImpactCategory {
  NUCLEAR_WASTE = "Nuclear Waste",
  CARBON_DIOXIDE = "Carbon Dioxide",
}
@Entity()
export class EnvironmentalImpact {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({
    type: "enum",
    enum: EnvironmentalImpactCategory,
    comment: "The environmental impact category of this value.",
  })
  category: EnvironmentalImpactCategory;

  @Column({
    nullable: true,
    comment: "Amount of this portion in g/kWh.",
  })
  amount: number;
}
