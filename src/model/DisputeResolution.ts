import {
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  Column,
  ManyToMany,
  ManyToOne,
} from "typeorm";
import { ChargingSession } from "./ChargingSession";
import { Transaction } from "./Transaction";

@Entity()
export class DisputeResolution {
  @PrimaryGeneratedColumn()
  id: string;

  @OneToOne(() => Transaction, (transaction) => transaction.id)
  @JoinColumn()
  transaction: Transaction;

  @Column()
  description: string;

  @ManyToOne(() => ChargingSession, (chargingsession) => chargingsession.id)
  chargingSession: ChargingSession;
}
