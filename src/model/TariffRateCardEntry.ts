import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  Timestamp,
} from "typeorm";
import { TariffRateCard } from "./TariffRateCard";

export enum TariffFactorTypeEnum {
  FIXED = "fixed",
  PERUNIT = "per unit",
}

@Entity()
export class TariffRateCardEntry {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  name: string;

  @Column({ type: "enum", enum: TariffFactorTypeEnum })
  type: TariffFactorTypeEnum;

  @Column()
  value: number;

  @Column()
  comment: string;

  @Column()
  startTime: Date;

  @Column()
  isElectricityendTime: Date;

  @Column()
  isElectricity: boolean;

  @ManyToOne(() => TariffRateCard, (tariffratecard) => tariffratecard.id)
  tariffRateCard: TariffRateCard;
}
