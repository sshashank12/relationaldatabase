import { Entity, PrimaryGeneratedColumn, Column, BaseEntity } from "typeorm";
import { Status } from "./ChargePoint";

@Entity()
export class StatusSchedule {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ comment: "Begin of the scheduled period." })
  periodBegin: Date;

  @Column({ comment: "End of the scheduled period, if known." })
  periodEnd: Date;

  @Column({ comment: "End of the scheduled period, if known." })
  status: Status;
}
