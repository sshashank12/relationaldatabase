import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { TransactionalData } from "./TransactionData";

export enum PaymentFrequency {
  MONTHLY = "Monthly",
  QUATERLY = "Quaterly",
  YEARLY = "Yearly",
}

@Entity()
export class PaymentDetails extends TransactionalData {
  @Column({
    type: "enum",
    enum: PaymentFrequency,
    comment: "Frequency of payment",
  })
  paymentFrequency: PaymentFrequency;

  @Column({ comment: "Account Number for the payment" })
  accountNumber: number;

  @Column({ length: 255, comment: "Account holder name" })
  accountHolderName: string;

  @Column({ length: 255, comment: "Name of bank in whick account is opened" })
  bankName: string;

  @Column({ length: 16, comment: "IFSC code of the band" })
  ifsc: string;
}
