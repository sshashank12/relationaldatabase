import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  OneToMany,
  ManyToOne,
  Check,
} from "typeorm";
import { ChargingSession } from "./ChargingSession";
import { Connector } from "./Connector";
import { GeoLocation } from "./GeoLocation";
import { Image } from "./Image";
import { Location } from "./Location";
import { SimCardDetail } from "./SimCardDetails";
import { Review } from "./Review";
import { ChargerConfiguration } from "./ChargerConfiguration";
import { WifiDetails } from "./WifiDetails";
import { LANDetails } from "./LANDetails";
import { LocalOrganisation } from "./LocalOrganisation";
import { OperatorOrganisation } from "./OperatorOrganisation";
import { StaticData } from "./StaticData";

export enum Status {
  AVAILABLE = "Available",
  BLOCKED = "Blocked",
  CHARGING = "Charging",
  INOPERATIVE = "Inoperative",
  OUTOFORDER = "Outoforder",
  PLANNED = "Planned",
  REMOVED = "Removed",
  RESERVED = "Reserved",
  UNKNOWN = "Unknown",
}

export enum RegistrationStatus {
  Active = "Active",
  Inactive = "Inactive",
  Blocked = "Blocked",
  Expired = "Expired",
  Pending = "Pending",
}

export enum ChargingStatus {
  Charging = "Charging",
  Available = "Available",
  Unavailable = "Unavailable",
  Unknown = "Unknown",
  Faulted = "Faulted",
  Preparing = "Preparing",
  Finishing = "Finishing",
  Connected = "Connected",
  SuspendedEV = "SuspendedEV",
  SuspendedEVSE = "SuspendedEVSE",
}

export enum ChargerModelType {
  Type2 = "Type2",
  BharatAC = "BharatAC",
}

@Entity()
// @Check('cafDocURL" == /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/')
export class ChargePoint extends StaticData {
  //OCPP Specifications

  @Column({ length: 36 })
  chargePointId: string;

  @Column({ comment: "This indicates the evseId" })
  chargePointName: string;

  @Column({ length: 255, comment: "This indicates the chargePointVendor" })
  chargePointVendor: string;

  @Column({ length: 10, comment: "This indicates the chargePointSerialNumber" })
  chargePointSerialNumber: string;

  @Column({ length: 255, comment: "This indicates the chargePointModel" })
  chargePointModel: string;

  @Column({ comment: "This indicates the ocppProtocol" })
  ocppProtocol: number;

  //Location Info
  @ManyToOne(() => Location, (location) => location.id)
  location: Location;

  @OneToMany(() => ChargingSession, (chargingsession) => chargingsession.id, {
    eager: true,
    cascade: true,
  })
  chargingsession: ChargingSession[];

  //Model Info
  @Column({ length: 50 })
  modelName: string;

  @Column({ length: 50 })
  modelSeries: string;

  @Column({ type: "enum", enum: ChargerModelType })
  modelType: ChargerModelType;

  @Column()
  modelPower: number;

  @Column({ nullable: true })
  suitableVehicleTypes: string;

  //Statuses
  @Column({ type: "enum", enum: RegistrationStatus })
  registractionStatus: RegistrationStatus;

  @Column({ type: "enum", enum: ChargingStatus })
  chargingStatus: ChargingStatus;

  //Geo Location
  @OneToOne(() => GeoLocation, { cascade: true, eager: true })
  @JoinColumn()
  coordinates: GeoLocation;

  @Column({ nullable: true })
  siteSurveyNumber: number;

  @Column({ nullable: true })
  siteSurveyDate: Date;

  //Charge Auto Config
  @OneToOne(
    () => ChargerConfiguration,
    (chargeconfiguration) => chargeconfiguration.id,
    { eager: true }
  )
  chargerConfiguration: ChargerConfiguration;

  @Column({ nullable: true })
  activationDate: Date;

  @Column({ nullable: true })
  warrantyPeriod: number;

  @Column({ nullable: true })
  warrantyDate: Date;

  @Column({ nullable: true })
  gunNumber: number;

  @Column({ nullable: true })
  socketNumber: number;

  //Socket Connection Info
  @Column()
  sessionOnline: boolean;

  @Column()
  sessionUpdatedAt: Date;

  @OneToMany(() => Connector, (connector) => connector.id)
  connectors: Connector[];

  @Column({ nullable: true })
  imagesPath: string;

  @Column()
  docPath: string;

  @Column()
  statusUpdatedAt: Date;

  @Column({ nullable: true })
  cafdocURL: string; //add url constraint

  @OneToOne(() => SimCardDetail, (sim) => sim.id)
  @JoinColumn()
  simCardDetails: SimCardDetail;

  @OneToOne(() => WifiDetails, (wifidetail) => wifidetail.id)
  @JoinColumn()
  wifiDetails: WifiDetails;

  @OneToOne(() => LANDetails, (landetail) => landetail.id)
  @JoinColumn()
  lanDetails: LANDetails;

  @OneToOne(() => Image, { cascade: true, eager: true })
  @JoinColumn()
  images: Image;

  load(data: ChargePoint): ChargePoint {
    super.load(data);
    this.chargePointId = data.chargePointId;
    this.chargePointName = data.chargePointName;
    this.chargePointVendor = data.chargePointVendor;
    this.chargePointSerialNumber = data.chargePointSerialNumber;
    this.chargePointModel = data.chargePointModel;
    this.ocppProtocol = data.ocppProtocol;
    this.location = data.location;
    this.chargingsession = data.chargingsession;
    this.modelName = data.modelName;
    this.modelSeries = data.modelSeries;
    this.modelType = data.modelType;
    this.modelPower = data.modelPower;
    this.suitableVehicleTypes = data.suitableVehicleTypes;
    this.registractionStatus = data.registractionStatus;
    this.chargingStatus = data.chargingStatus;
    this.siteSurveyNumber = data.siteSurveyNumber;
    this.siteSurveyDate = data.siteSurveyDate;
    this.chargerConfiguration = data.chargerConfiguration;
    this.activationDate = data.activationDate;
    this.warrantyPeriod = data.warrantyPeriod;
    this.warrantyDate = data.warrantyDate;
    this.gunNumber = data.gunNumber;
    this.socketNumber = data.socketNumber;
    this.sessionOnline = data.sessionOnline;
    this.sessionUpdatedAt = data.sessionUpdatedAt;
    this.connectors = data.connectors;
    this.imagesPath = data.imagesPath;
    this.docPath = data.docPath;
    this.statusUpdatedAt = data.statusUpdatedAt;
    this.cafdocURL = data.cafdocURL;
    this.simCardDetails = data.simCardDetails;
    this.wifiDetails = data.wifiDetails;
    this.lanDetails = data.lanDetails;
    this.images = data.images;
    return this;
  }
}
