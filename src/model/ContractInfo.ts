import { extend } from "lodash";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { StaticData } from "./StaticData";

@Entity()
export class ContractInfo extends StaticData {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  lcpShare: number;

  @Column()
  contractOwnerShare: number;

  //One to one with location not session
}
