import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { ChargingSession } from "./ChargingSession";
import { DisputeResolution } from "./DisputeResolution";
import { Transaction } from "./Transaction";
import { TransactionalData } from "./TransactionData";
import { Wallet } from "./Wallet";

export enum DisputeType {
  CHARGINGSESSION = "ChargingSession",
  WALLETRECONCILIATION = "WalletReconciliation",
}

export enum DisputeStatus {
  RESOLVED = "Resolved",
  CREATED = "Created",
  PENDING = "Pending",
  REJECTED = "Rejected",
  CANCELED = "Canceled",
}

@Entity()
export class Dispute extends TransactionalData {
  @Column({ type: "enum", enum: DisputeType })
  type: DisputeType;

  @Column({ type: "enum", enum: DisputeStatus })
  status: DisputeStatus;

  @Column()
  description: string;

  @OneToOne(() => ChargingSession, (chargingsession) => chargingsession.id)
  chargingSessionId: ChargingSession;

  @OneToOne(() => Wallet, (wallet) => wallet.id)
  @JoinColumn()
  wallet: Wallet;

  @OneToOne(() => Transaction, (transaction) => transaction.id)
  @JoinColumn()
  transaction: Transaction;

  @Column()
  raisedByUserId: string;

  @Column()
  raisedByUserName: string;

  @Column()
  fieldsToBeUpdated: string;

  @Column()
  previousValues: string;

  @Column()
  rateUnit: string;

  @Column()
  disputeId: string;

  @Column()
  rejectDescription: string;

  @Column()
  memberPhoneNumber: string;

  @OneToOne(
    () => DisputeResolution,
    (disputeresolution) => disputeresolution.id
  )
  @JoinColumn()
  disputeResolution: DisputeResolution;
}
