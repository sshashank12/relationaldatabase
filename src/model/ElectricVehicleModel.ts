import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { StaticData } from "./StaticData";

@Entity()
export class ElectricVehicleModel extends StaticData {
  @Column()
  manufacturer: string;

  @Column()
  shortCode: string;

  @Column()
  model: string;

  @Column()
  vehicleType: number;

  @Column()
  variant: string;

  @Column()
  range: number;

  @Column()
  chargeTime: Date;

  @Column()
  connectorType: string;

  @Column()
  batteryCapacity: number;

  @Column()
  imagesPath: string;

  @Column()
  supportFastCharging: boolean;

  @Column()
  batterySwappable: boolean;

  @Column({ array: true })
  connectorTypesSupported: string;

  @Column()
  additionalInfo: string;
}
