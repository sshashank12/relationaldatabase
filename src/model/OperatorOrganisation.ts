import { ChildEntity, Column, OneToMany } from "typeorm";
import { Location } from "./Location";
import { Organisation } from "./Organisation";

@ChildEntity()
export class OperatorOrganisation extends Organisation {
  @OneToMany(() => Location, (Location) => Location.id)
  location: Location[];

  load(data: OperatorOrganisation): OperatorOrganisation{
    super.load(data)
    this.location = data.location
    return this
  }
}
