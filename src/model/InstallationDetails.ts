import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
} from "typeorm";
import { ChargePoint } from "./ChargePoint";
import { StaticData } from "./StaticData";
@Entity()
export class InstallationDetails extends StaticData {
  @PrimaryGeneratedColumn()
  id: string;

  @OneToOne(() => ChargePoint, (scd) => scd.chargePointId, {
    cascade: false,
    lazy: true,
  })
  @JoinColumn()
  chargePointId: ChargePoint;

  @Column({ nullable: true })
  chargePointDocId: string;

  @Column({ nullable: true })
  ipName: string;

  @Column({ nullable: true })
  ipContactNumber: string;

  @Column({ nullable: true })
  ipEmail: string;

  @Column({ nullable: true })
  cableMeterSpecifications: string;

  @Column({ nullable: true })
  mcb: string;

  @Column({ nullable: true })
  cableType: string;

  @Column({ nullable: true })
  cableLength: string;

  @Column({ nullable: true })
  cableMake: string;

  @Column({ nullable: true })
  installationDate: Date;

  @Column({ nullable: true })
  submeter: boolean;

  @Column({ nullable: true })
  basement: number;

  @Column({ nullable: true })
  pole: boolean;

  @Column({ nullable: true })
  civilWork: boolean;

  @Column({ nullable: true })
  civilWorkType: string;

  @Column({ nullable: true })
  canopy: boolean;
}
