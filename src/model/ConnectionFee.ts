import { Min, min } from "class-validator";
import {
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  JoinColumn,
} from "typeorm";
import { TransactionalData } from "./TransactionData";

export enum ConnectionFeeFlag {
  Active = "Active",
  Inactive = "Inactive",
}

@Entity()
export class ConnectionFee extends TransactionalData {
  @Column()
  // @OneToMany(() => ChargepointIdModel, (chargepointIdModel) => address.id)
  // @JoinColumn()
  chargepointIdModelId: string; //Link it to the chargepoint model id of the charge point model

  @Column()
  @Min(0)
  connectionFee: number;

  @Column()
  @Min(0)
  minimumBalance: number;

  @Column()
  comment: string;

  @Column({ type: "enum", enum: ConnectionFeeFlag })
  flag: ConnectionFeeFlag;
}
