import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { TariffRateCard } from "./TariffRateCard";

@Entity()
export class TariffRateFlatEntry {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  fixedValue: number;

  @Column()
  perUnitValue: number;

  @Column()
  startHour: Date;

  @Column()
  endHour: Date;

  @Column()
  electricityRate: number;

  @ManyToOne(() => TariffRateCard, (tariffratecard) => tariffratecard.id)
  tariffRateCard: TariffRateCard;
}
