import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { ChargePoint } from "./ChargePoint";
import { Location } from "./Location";
import { Member } from "./Member";
import { Organisation } from "./Organisation";
import { StaticData } from "./StaticData";
import { UserProfile } from "./User";

@Entity()
export class Review extends StaticData {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ nullable: true, length: 255 })
  message: string;

  @Column()
  rating: number;


  @Column({ nullable: true })
  userName: string;

  @Column()
  date: Date;

  @ManyToOne(() => Location, location => location.reviews)
  location: Location;

  // @Column({ length: 48 })
  // locationShortName: string;

  // @Column({ nullable: true })
  // organisationId: string;

  // @Column({ nullable: true })
  // organisationShortName: string;

  @OneToOne(() => Organisation, (organisation) => organisation.id)
  @JoinColumn()
  organisation: Organisation;

  @Column()
  isPublic: boolean;

  @Column({ nullable: true, length: 48 })
  title: string;

  @Column({ nullable: true, length: 255 })
  response: string;

  @Column({ nullable: true })
  imagePath: string;

  @ManyToOne(() => Member, (member) => (member.userId))
  user: Member

}
