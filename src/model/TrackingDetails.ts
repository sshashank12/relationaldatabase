import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { StaticData } from "./StaticData";

@Entity()
export class TrackingDetails extends StaticData {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  trackingId: string;

  @Column()
  deliveryVendor: string;

  @Column()
  trackingLink: string;
}
