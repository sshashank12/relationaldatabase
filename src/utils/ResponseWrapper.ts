import { Request, Response, NextFunction } from 'express'
import { UpdateResult } from 'typeorm';
import { X_REQUEST_ID } from './config';

export const SuccessWrapper = (statusMessage: string, data?: any) => {
  return {
    status: "SUCCESS",
    statusMessage: statusMessage,
    data: data,
    timestamp: new Date(),
  };
};

export const ErrorWrapper = (statusMessage: string) => {
  return {
    status: "FAILED",
    statusMessage: statusMessage,
    timestamp: new Date(),
  };
};

export function handleError(err: Error, req: Request, res: Response, next: NextFunction) {
  res.status(500)
  res.json(ErrorWrapper(err.message))
}

export function handleRequest(req: Request, res: Response, next: NextFunction) {
  const requestId = req.header(X_REQUEST_ID)
  if(requestId)
    res.setHeader(X_REQUEST_ID, requestId);
  else
    res.status(422).send(ErrorWrapper('Header missing X-Request-ID'))
  next()
}
