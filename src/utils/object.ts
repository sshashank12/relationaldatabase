import { Transform } from "stream"
import { pickBy, transform, isEqual, isObject, merge } from 'lodash'

export function difference(object: any, base: any) {
    return transform(object, (result: any, value: any, key: string) => {
      if (value !== undefined && !isEqual(value, base[key])) {
        result[key] = isObject(value) && isObject(base[key]) && !base[key]['_seconds'] ? difference(value, base[key]) : value
      }
    })
  }