import { ChargerConfiguration } from "../model/ChargerConfiguration";
import { AppDataSource } from "../repository/Database";

// CREATE

export async function createChargerConfig(chargerconfig: ChargerConfiguration) {
    return await AppDataSource.getRepository(ChargerConfiguration).save(chargerconfig);
  }
  