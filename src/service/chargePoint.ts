import { ChargePoint } from "../model/ChargePoint";
import { AppDataSource } from "../repository/Database";

// CREATE

export async function createChargePoint(cp: ChargePoint) {
  return await AppDataSource.getRepository(ChargePoint).save(cp);
}

// UPDATE

export async function updateChargePoint(id: any, cp: any) {
  const result = await AppDataSource.createQueryBuilder()
    .update(ChargePoint)
    .set(cp)
    .where("chargePointId = :id", { id: id })
    .execute();
    if (result && result.affected === 1) return await getChargePoint(id);
    else throw new Error("CP Update Unsuccessful");
}

// GET (cp)

export async function getAllChargePoint(data:{limit?: any, offset?: any}) {
  return await AppDataSource.getRepository(ChargePoint)
    .createQueryBuilder()
    .limit(data.limit ? parseInt(data.limit) : 10)
    .offset(data.offset ? parseInt(data.offset) : 0)
    .getManyAndCount();
}

export async function getChargePoint(id: any) {
  return await AppDataSource.getRepository(ChargePoint).findOneBy({id: id})
}

// DELETE

export async function deleteChargePoint(id: any) {
  return await AppDataSource.getRepository(ChargePoint)
    .createQueryBuilder()
    .delete()
    .from(ChargePoint)
    .where("chargePointId = :id", { id: id })
    .execute();
}
