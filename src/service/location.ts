// CREATE

import { Location } from "../model/Location";
import { AppDataSource } from "../repository/Database";

// CREATE

export async function createLocation(loc: Location) {
  return await AppDataSource.getRepository(Location).save(loc);
}

//GET
export async function getAllLocation(data: {limit?: any, offset?: any}) {
  return await AppDataSource.getRepository(Location)
    .createQueryBuilder()
    .limit(data.limit ? parseInt(data.limit) : 10)
    .offset(data.offset ? parseInt(data.offset) : 0)
    .getManyAndCount();
}

export async function getLocation(id: any) {
  return await AppDataSource.getRepository(Location).findOneBy({locationId: id});
}

// UPDATE

export async function updateLocation(id: any, loc: Location) {
  const result = await AppDataSource.createQueryBuilder()
    .update(Location)
    .set(loc)
    .where("locationId = :id", {id: id })
    .execute();
    if (result && result.affected === 1) return await getLocation(id);
    else throw new Error("Location Update Unsuccessful");
}


// DELETE

export async function deleteLocation(id: any) {
  return await AppDataSource.getRepository(Location)
    .createQueryBuilder()
    .delete()
    .from(Location)
    .where("locationId = :id", { id: id })
    .execute();
}
