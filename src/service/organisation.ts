import { LocalOrganisation } from "../model/LocalOrganisation";
import { OperatorOrganisation } from "../model/OperatorOrganisation";
import { ServiceProviderOrganisation } from "../model/ServiceProviderOrganisation";
import { AppDataSource } from "../repository/Database";

// CREATE

export async function createLCP(lcp: LocalOrganisation) {
  return await AppDataSource.getRepository(LocalOrganisation).save(lcp);
}

export async function createEMSP(emsp: ServiceProviderOrganisation) {
  return await AppDataSource.getRepository(ServiceProviderOrganisation).save(
    emsp
  );
}

export async function createCPO(cpo: OperatorOrganisation) {
  return await AppDataSource.getRepository(OperatorOrganisation).save(cpo);
}

// UPDATE

export async function updateCPO(id: any, cpo: any) {
  const result = await AppDataSource.createQueryBuilder()
    .update(OperatorOrganisation)
    .set(cpo)
    .where("id = :id", { id: id }) 
    .execute();
  if (result && result.affected === 1) return await getCPO(id);
  else throw new Error("CPO Update Unsuccessful");
}

export async function updateLCP(id: any, lcp: any) {
  const result = await AppDataSource.createQueryBuilder()
    .update(LocalOrganisation)
    .set(lcp)
    .where("id = :id", { id: id })
    .execute();
    if (result && result.affected === 1) return await getLCP(id);
    else throw new Error("LCP Update Unsuccessful");
}

export async function updateEMSP(id: any, emsp: any) {
  const result = await AppDataSource.createQueryBuilder()
    .update(ServiceProviderOrganisation)
    .set(emsp)
    .where("id = :id", { id: id })
    .execute();
    if (result && result.affected === 1) return await getEMSP(id);
    else throw new Error("EMSP Update Unsuccessful");
}

// GET & GETAll (LCP)
export async function getAllLCP(data:{limit?: any, offset?: any}) {
  return await AppDataSource.getRepository(LocalOrganisation).find()
}

export async function getLCP(id: any) {
  return await AppDataSource.getRepository(LocalOrganisation).findOneBy({id: id})
}

// GET & GETAll (CPO)

export async function getAllCPO(data:{limit?: any, offset?: any}) {
  return await AppDataSource.getRepository(OperatorOrganisation)
    .createQueryBuilder()
    .limit(data.limit ? parseInt(data.limit) : 10)
    .offset(data.offset ? parseInt(data.offset) : 0)
    .getManyAndCount();
}

export async function getCPO(id: any) {
  return await AppDataSource.getRepository(OperatorOrganisation).findOneBy({id:id})
}

//GET & GETAll (EMSP)

export async function getAllEMSP(data: {limit?: any, offset?: any}) {
  return await AppDataSource.getRepository(ServiceProviderOrganisation)
    .createQueryBuilder()
    .limit(data.limit ? parseInt(data.limit) : 10)
    .offset(data.offset ? parseInt(data.offset) : 0)
    .getManyAndCount();
}

export async function getEMSP(id: any) {
  return await AppDataSource.getRepository(ServiceProviderOrganisation).findOneBy({id: id})
}

// DELETE (EMSP)

export async function deleteEMSP(id: any) {
  return await AppDataSource.getRepository(ServiceProviderOrganisation)
    .createQueryBuilder()
    .delete()
    .from(ServiceProviderOrganisation)
    .where("id = :id", { id: id })
    .execute();
}

// DELETE (CPO)

export async function deleteCPO(id: any) {
  return await AppDataSource.getRepository(OperatorOrganisation)
    .createQueryBuilder()
    .delete()
    .from(OperatorOrganisation)
    .where("id = :id", { id: id })
    .execute();
}

// DELETE (LCP)

export async function deleteLCP(lcp: LocalOrganisation, id: any) {
  return await AppDataSource.getRepository(LocalOrganisation)
    .createQueryBuilder()
    .delete()
    .from(LocalOrganisation)
    .where("id = :id", { id: id })
    .execute();
}
