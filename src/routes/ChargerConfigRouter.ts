import { Router, Request, Response, NextFunction } from "express";
import { ChargerConfiguration } from "../model/ChargerConfiguration";
import { createChargerConfig } from "../service/chargerConfig";
import { SuccessWrapper } from "../utils/ResponseWrapper";

export const ChargerConfigRouter = Router();

// PUT

ChargerConfigRouter.put(
    "/create",
    async (req: Request, res: Response, next: NextFunction) => {
      try {
        const chargeconfig = new ChargerConfiguration().load(req.body);
        res.status(201).send(SuccessWrapper('', await createChargerConfig(chargeconfig)));
      } catch (err: any) {
        next(err);
      }
    }
  );