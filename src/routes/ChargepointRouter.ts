import { Router, Request, Response, NextFunction } from "express";
import { ChargePoint } from "../model/ChargePoint";
import { ErrorWrapper, SuccessWrapper } from "../utils/ResponseWrapper";
import { AppDataSource } from "../repository/Database";
import {
  createChargePoint,
  deleteChargePoint,
  getAllChargePoint,
  getChargePoint,
  updateChargePoint,
} from "../service/chargePoint";
import { limitCount, X_LIMIT, X_TOTAL_COUNT } from "../utils/config";

export const ChargePointRouter = Router();

// PUT

ChargePointRouter.put(
  "/cp/create",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const cp = new ChargePoint().load(req.body);
      res.status(201).send(SuccessWrapper('', await createChargePoint(cp)));
    } catch (err: any) {
      next(err);
    }
  }
);

// PATCH

ChargePointRouter.patch(
  "/list/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const updatedcp = await updateChargePoint(req.query.id, req.body);
      res.status(200).send(SuccessWrapper("CP Updated", updatedcp));
    } catch (err: any) {
      next(err);
    }
  }
);

// GET

ChargePointRouter.get(
  "/list",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const [cpList, count] = await getAllChargePoint({});
      res.set("X-Total-Count");
      res.set("X-Limit", limitCount);
      res.status(200).send(SuccessWrapper("All Charging Stations", cpList));
    } catch (err: any) {
      next(err);
    }
  }
);

ChargePointRouter.get(
  "/list/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const cp = await getChargePoint(req.query.id);
      res.set(X_TOTAL_COUNT);
      res.set(X_LIMIT, limitCount);
      res.status(200).send(SuccessWrapper('', cp));
    } catch (err: any) {
      next(err);
    }
  }
);

// DELETE

ChargePointRouter.delete(
  "/list/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const cp = await deleteChargePoint(req.query.id);
      res.send(SuccessWrapper("Charge Point deleted", cp));
    } catch (err: any) {
      next(err);
    }
  }
);

function handleError(
  res: Response<any, Record<string, any>>,
  err: any
): void | PromiseLike<void> {
  throw new Error("Function not implemented.");
}
