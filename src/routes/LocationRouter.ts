import { Router, Request, Response, NextFunction } from "express";
import { Location } from "../model/Location";
import {
  createLocation,
  deleteLocation,
  getAllLocation,
  getLocation,
  updateLocation,
} from "../service/location";
import { limitCount, X_LIMIT, X_TOTAL_COUNT } from "../utils/config";
import { ErrorWrapper, SuccessWrapper } from "../utils/ResponseWrapper";

export const locationRouter = Router();

// PUT

locationRouter.put(
  "/create",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      console.log("api");
      const location = new Location().load(req.body);
      res.status(201).send(SuccessWrapper('', await createLocation(location)))
    } catch (err: any) {
      next(err);
    }
  }
);

// GET

locationRouter.get(
  "/list",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const [locList, count] = await getAllLocation({});
      res.set(X_TOTAL_COUNT);
      res.set(X_LIMIT, limitCount);
      res.status(200).send(SuccessWrapper("All Locations", locList));
    } catch (err: any) {
      next(err);
    }
  }
);

locationRouter.get(
  "/list/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const loc = await getLocation(req.query.id);
      res.set(X_TOTAL_COUNT);
      res.set(X_LIMIT, limitCount);
      res.status(200).send(SuccessWrapper('', loc));
    } catch (err: any) {
      next(err);
    }
  }
);

// UPDATE

locationRouter.patch(
  "/list/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const updatedloc = await updateLocation(req.query.id, req.body);
      res.status(200).send(SuccessWrapper("Location Updated", updatedloc));
    } catch (err: any) {
      next(err);
    }
  }
);

// DELETE
locationRouter.delete(
  "/list/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const loc = await deleteLocation(req.query.id);
      res.status(200).send(SuccessWrapper("Location deleted", loc));
    } catch (err: any) {
      next(err);
    }
  }
);
