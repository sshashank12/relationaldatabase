import { Router, Request, Response, NextFunction } from "express";
import { LocalOrganisation } from "../model/LocalOrganisation";
import { OperatorOrganisation } from "../model/OperatorOrganisation";
import { Organisation } from "../model/Organisation";
import { ServiceProviderOrganisation } from "../model/ServiceProviderOrganisation";
import { AppDataSource } from "../repository/Database";
import {
  createCPO,
  createEMSP,
  createLCP,
  deleteCPO,
  deleteEMSP,
  deleteLCP,
  getAllCPO,
  getAllEMSP,
  getAllLCP,
  getCPO,
  getEMSP,
  getLCP,
  updateCPO,
  updateEMSP,
  updateLCP,
} from "../service/organisation";
import { limitCount, X_LIMIT, X_TOTAL_COUNT } from "../utils/config";
import { ErrorWrapper, SuccessWrapper } from "../utils/ResponseWrapper";

export const organisationRouter = Router();

// PUT
organisationRouter.put(
  "/lcp/create",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const org = new LocalOrganisation().load(req.body);
      res.status(201).send(await createLCP(org))
    } catch (err: any) {
      next(err);
    }
  }
);

organisationRouter.put(
  "/emsp/create",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const org = new ServiceProviderOrganisation().load(req.body);
      res.status(201).send(SuccessWrapper('EMSP Created', await createEMSP(org)));
    } catch (err: any) {
      next(err);
    }
  }
);

organisationRouter.put(
  "/cpo/create",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const org = new OperatorOrganisation().load(req.body);
      res.status(201).send(SuccessWrapper('', await createCPO(org)));
    } catch (err: any) {
      next(err);
    }
  }
);

// PATCH

organisationRouter.patch(
  "/cpo/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const updatedCPO = await updateCPO(req.query.id, req.body);
      console.log(updatedCPO);
      res.status(200).send(SuccessWrapper('', updatedCPO));
    } catch (err: any) {
      next(err);
    }
  }
);

organisationRouter.patch(
  "/lcp/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const updatedLCP = await updateLCP(req.query.id, req.body);
      res.status(200).send(SuccessWrapper("LCP Updated", updatedLCP));
    } catch (err: any) {
      next(err);
    }
  }
);

organisationRouter.patch(
  "/emsp/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const updatedEMSP = await updateEMSP(req.query.id, req.body)
      res.status(200).send(SuccessWrapper("EMSP Updated", updatedEMSP));
    } catch (err: any) {
      next(err);
    }
  }
);

// GET (LCP)

organisationRouter.get(
  "/lcp",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const [lcpList, count] = await getAllLCP({});
      res.set(X_TOTAL_COUNT);
      res.set(X_LIMIT, "50");
      res.send(SuccessWrapper("All LCP", lcpList));
    } catch (err: any) {
      next(err);
    }
  }
);

organisationRouter.get(
  "/lcp/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const lcp = await getLCP(req.query.id)
      console.log(lcp, 'lcp aa gya');
      
      res.send(SuccessWrapper("LCP data", lcp));
    } catch (err: any) {
      next(err);
    }
  }
);

// GET (CPO)

organisationRouter.get(
  "/cpo",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const [cpoList, count] = await getAllCPO({});
      res.set(X_TOTAL_COUNT, count.toString());
      res.set(X_LIMIT, "50");
      res.send(SuccessWrapper("All cpo", cpoList));
    } catch (err: any) {
      next(err);
    }
  }
);

organisationRouter.get(
  "/cpo/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const cpo = await getCPO(req.query.id);
      res.set(X_TOTAL_COUNT);
      res.set(X_LIMIT, limitCount);
      res.send(SuccessWrapper("CPO data", cpo));
    } catch (err: any) {
      next(err);
    }
  }
);

// GET (EMSP)

organisationRouter.get(
  "/emsp",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const [emspList, count] = await getAllEMSP({});
      res.set(X_TOTAL_COUNT, count.toString());
      res.set(X_LIMIT, limitCount);
      res.send(SuccessWrapper("All emsp", emspList));
    } catch (err: any) {
      next(err);
    }
  }
);

organisationRouter.get(
  "/emsp/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const emsp = await getEMSP(req.query.id);
      res.set(X_TOTAL_COUNT);
      res.set(X_LIMIT, limitCount);
      res.send(SuccessWrapper("EMSP data", emsp));
    } catch (err: any) {
      next(err);
    }
  }
);

// DELETE EMSP
organisationRouter.delete(
  "/emsp/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const emsp = await deleteEMSP(req.query.id);
      res.send(SuccessWrapper("EMSP deleted", emsp));
    } catch (err: any) {
      next(err);
    }
  }
);

// DELETE CPO
organisationRouter.delete(
  "/cpo/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const cpo = await deleteCPO(req.query.id);
      res.send(SuccessWrapper("CPO deleted", cpo));
    } catch (err: any) {
      next(err);
    }
  }
);

// DELETE LCP
organisationRouter.delete(
  "/lcp/:id",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const lcp = await deleteLCP(req.body, req.query.id);
      res.send(SuccessWrapper("LCP deleted", lcp));
    } catch (err: any) {
      next(err);
    }
  }
);

function handleError(
  res: Response<any, Record<string, any>>,
  err: any
): void | PromiseLike<void> {
  throw new Error("Function not implemented.");
}
